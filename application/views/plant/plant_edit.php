<?php $this->load->view("includes/header.php"); ?>
<form method="post" action="<?=base_url();?>engine/crud_plant/edit_proses" enctype="multipart/form-data"/>
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Update Plant Form</h2>
					<div class="well well-small">
                                            <table class="table">
                                                    <tr>
                                                        <td width="200px">Plant Name</td><input type='hidden' value="<?=$list->id;?>" name="id"/>
                                                        <td><input type="text" required name="plant_name" class="span6" value="<?=$list->plant_name;?>" required/> * input without word's 'Plant'</td>
                                                    </tr>
                                            </table>
                                            <button type="submit" class="btn"><i class="icon-check icon-black"></i> Save</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>

<script type="text/javascript">
    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah1')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah2')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>