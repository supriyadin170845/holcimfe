<?php
	$this->load->view('includes/header.php');
?>

<div id="main">
	<div id="content">
		<div class="inner">
			<div class="row-fluid">
				<div class="span12 header-map-detil">
					<div class="span1 status-sever">
						<div class="yellow"></div>
					</div>
					<div class="span2 status-haccode">
						<h2>491-AC5</h2>
					</div>
					<div class="span4">
						<div id="pic-container">
							<div class="first">
								<img src="https://s3.amazonaws.com/uifaces/faces/twitter/divya/128.jpg" class="thumb">
								<div class="name">Ratnasari</div>
								<div class="nip">ID: 2948573632</div>
							</div>
							<div class="second">
								<img src="https://s3.amazonaws.com/uifaces/faces/twitter/gt/128.jpg" class="thumb">
								<div class="name">Budi Saputri</div>
								<div class="nip">ID: 2948573691</div>
							</div>
						</div>
					</div>
					<div class="span2 inspection">
						<span class="icon-wear icon-inspection-top"></span><p>&nbsp;Wear Msr.</p>
					</div>
					<div class="span3">
						<div class="jump-btn">
							<div class="btn-group">
								<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Other Reports&nbsp;&nbsp;<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thermo">Thermography</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_stop">Stop Inspection</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_inspection">Inspection Report</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_vibration">Vibration Analysis</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thickness_general">General Thickness</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thickness_kiln">Kiln Thickness</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thickness_stack">Stack Thickness</a></li>
								</ul>
							</div>
						</div>
						<div class="pull-right back-btn">
							<button onclick="history.go(-1);" class="btn"><i class="icon-chevron-left"></i>Back</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer2"></div>
			</div>
			<div class="row-fluid">
				<div class="span4 sparepart">
					<img src="<?=base_url()?>application/views/assets/report/img/area.jpg" width="100%">
					<div class="well">
					<h3>Spare Part List</h3>
					<div id="accordion1" class="accordion">
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" href="#collapseOne" data-toggle="collapse" data-parent="#accordion1">Head Sprocket</a>
							</div>
							<div id="collapseOne" class="accordion-body in collapse">
								<div class="accordion-inner">
									<p>Lorem ipsum</p>
									<p>dolor sit amet</p>
								</div>
							</div>
						</div>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" href="#collapseTwo" data-toggle="collapse" data-parent="#accordion1">Tail Sprocket</a>
							</div>
							<div id="collapseTwo" class="accordion-body collapse">
								<div class="accordion-inner">
									<p>Lorem ipsum</p>
									<p>dolor sit amet</p>
								</div>
							</div>
						</div>
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" href="#collapseThree" data-toggle="collapse" data-parent="#accordion1">Bearing</a>
							</div>
							<div id="collapseThree" class="accordion-body collapse">
								<div class="accordion-inner">
									<p>Lorem ipsum</p>
									<p>dolor sit amet</p>
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>
				<div class="span8">
					<div id="container" style="min-width: 310px; height: 500px; margin: 0 auto"></div>
				</div>					
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Remarks</h3>
						<div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse faucibus rutrum adipiscing. Donec sollicitudin ultricies dignissim. Aenean blandit, lacus vel aliquet bibendum, ante libero lacinia purus, quis dictum metus purus id ante. Nam rhoncus neque ut magna gravida, non faucibus tortor porttitor. Mauris ultrices ligula sit amet libero porttitor auctor. Donec gravida elementum posuere. Aliquam scelerisque quam sit amet lorem hendrerit porta. Duis bibendum dapibus orci. Vivamus in libero hendrerit, auctor velit quis, imperdiet odio. Duis hendrerit velit eget pretium feugiat. Quisque metus eros, dictum eget faucibus vel, tincidunt ac est. Sed dapibus pretium sollicitudin.</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Recommendation</h3>
						<div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse faucibus rutrum adipiscing. Donec sollicitudin ultricies dignissim. Aenean blandit, lacus vel aliquet bibendum, ante libero lacinia purus, quis dictum metus purus id ante. Nam rhoncus neque ut magna gravida, non faucibus tortor porttitor. Mauris ultrices ligula sit amet libero porttitor auctor. Donec gravida elementum posuere. Aliquam scelerisque quam sit amet lorem hendrerit porta. Duis bibendum dapibus orci. Vivamus in libero hendrerit, auctor velit quis, imperdiet odio. Duis hendrerit velit eget pretium feugiat. Quisque metus eros, dictum eget faucibus vel, tincidunt ac est. Sed dapibus pretium sollicitudin.</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Report List</h3>
						<div class="well">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>DATE</th>
										<th>ACTION</th>
										<th>NOTES</th>
										<th>OTHER</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>14/02/2014</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
									<tr>
										<td>13/02/2014</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
									<tr>
										<td>11/02/2014</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
									<tr>
										<td>10/02/2014</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
									<tr>
										<td>05/02/2014</td>
										<td>Lorem ipsum dolor sit amet</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</div>

<?php 
$this->load->view('includes/footer.php');
?>		

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/highcharts.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/modules/exporting.js"></script>	

<script type="text/javascript">
$(function () {
	$('#container').highcharts({
		title: {
			text: 'Severity Level Trend',
			x: -20 //center
		},
		subtitle: {
			text: '',
			x: -20
		},
		xAxis: {
			categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
				'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
		},
		yAxis: {
			title: {
				text: 'Severity Level'
			},
             tickInterval: 1,
				  min: 0,
				  max: 3
            
		}
        ,
		tooltip: {
			valueSuffix: ' '
		},
		legend: {
			layout: 'horizontal',
			align: 'center',
			verticalAlign: 'bottom',
			borderWidth: 0
		},
		series: [{
			name: 'HAC',
                        
			data: [{y:0,color:'#00ad00'}, {y:1,color:'#FAFA00'}, {y:1,color:'#FAFA00'}, {y:1,color:'#FAFA00'}, {y:2,color:'#BF0B23'}, {y:1,color:'#FAFA00'}, {y:0,color:'#00ad00'}, {y:1,color:'#FAFA00'}, {y:2,color:'#BF0B23'}, {y:1,color:'#FAFA00'}, {y:2,color:'#BF0B23'}]
		}]
	});
});


</script>