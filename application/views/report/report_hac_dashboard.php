<?php
	$this->load->view('includes/header.php');
?>
<style>
    a.tt{
        position: relative;
        z-index: 24;
        text-decoration: none;
    }
    a.tt span{
        display: none;
    }
    a.tt:hover{
        z-index: 25;
    }
    a.tt:hover span.tooltipx{
        display: block;
        position: absolute;
        //top: 10px; left:0;
        //padding: 0 20px 50px 0;
        //width: 200px;
        margin-left: 54px;
        margin-top: -52px;
        color: #993300;
        text-align: center;
    }
</style>
<style>
  .pagination{
    text-align: right;
    padding: 20px 0 5px 0;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.pagination a{
    margin: 0 5px 0 0;
    padding: 3px 6px;
    background: #B3B4BD;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.number{
    border: 1px solid #ddd;
}

.pagination a.current{
    background: #4D6F94;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.curren.hover{
    text-decoration: underline;
}
</style>
<div id="main">
	<div id="content">
		<div class="inner">
			<div class="row-fluid">
				<div class="span12 header-map-detil">
					<div class="span1 status-sever">
                    <?php 
                    $sql2a=$this->db->query("select * from hac where id='$id'
                                                 and severity_level not like '%2%' and severity_level like '%1%'")->result();
                        $sql2ax=$this->db->query("select * from hac where id='$id'
                                                 and severity_level LIKE '%2%'")->result();
                        $jum1=count($sql2a);
                        $jum2=count($sql2ax);
                        if($jum2 > 0){
                            echo "<div class='red'></div>";
                        }elseif($jum2 = 0 || $jum1 > 0){
                            echo "<div class='yellow'></div>";
                        }elseif($jum1 == 0 && $jum2 == 0){
                            echo "<div class='green'></div>";
                        }
                        ?>
					</div>
					<div class="span2 status-haccode">
						<h4><?php echo $hac_code;?></h4>
					</div>
					<div class="span4">
						<div id="pic-container">
                       
                            <?php 
                            if(count($top)==0){
                                
                            }else{
                            foreach($top as $top_row) :?>
                                <div style="float: left;padding-left: 3px;margin-top: -10px;">	    
                                    <a href="#" class="tt"><img src="<?php echo base_url();?>media/images/<?php echo $top_row->photo;?>" width="50px" height="50px" style="border: 1px solid;"/>
                                    <span class="topx"></span>
                                    <span class="tooltipx">
                                        <img src="<?php echo base_url();?>media/images/<?php echo $top_row->photo;?>" style="width: 150px;height: 150px;" class="img-responsive" style="border: 1px solid;"/>
                                        <div style="background-color: grey;font-weight: bolder;color: white;text-transform: capitalize;text-align: left;">&nbsp;NIP : <?php echo $top_row->nip;?></div>
                                        <div style="background-color: grey;font-weight: bolder;color: white;text-transform: capitalize;text-align: left;">&nbsp;Nama : <?php echo $top_row->nama;?></div>
                                    </span>
                                    <span class="bottomx"></span></a>
                                </div>	
                            <?php endforeach;?>
                            <?php } ?>
						</div>
					</div>
					<div class="span2 pull-right back-btn">
<!--						<a href="<?=base_url()?>report/main_report/area/<?php echo $hac_area;?>" class="btn"><i class="icon-chevron-left"></i>Back</a>-->
                                                <a href="<?=base_url()?>" class="btn"><i class="icon-chevron-left"></i>Back</a>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer2"></div>
			</div>
			<div class="row-fluid">
				<div class="span4 sparepart">
				<img src="<?=base_url()?>media/images/<?php echo $hac_image;?>" width="100%"/>
                                        <h3>Spare Part List</h3>
                                        <div class="well" style="overflow-y: scroll;height: 200px;">
                    
					
                    <?php if (count($assembly) == ''){
                        echo"<div class='accordion-group'>";
                                echo"<div class='accordion-heading'>";
                                echo"<a class='accordion-toggle' href='#collapseOne' data-toggle='collapse' data-parent='#accordion1'></a>";
                                echo"</div>";
                                echo"<div id='collapseOne' class='accordion-body collapse'></div></div>";
                        }
                        ?>
                        <?php foreach($assembly as $assembly_row) : ?>
					<div id="accordion1" class="accordion">
                    
                    
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" href="#collapseOne" data-toggle="collapse" data-parent="#accordion1"><?php echo $assembly_row->assembly_name;?></a>
							</div>
							<div id="collapseOne" class="accordion-body collapse">
                            <?php foreach($component as $component_row) : ?>
								<div class="accordion-inner">
									<p><?php echo $component_row->component_code;?></p>
									
								</div>
                                <?php endforeach; ?>
							</div>
						</div>
					</div>
                    <?php endforeach; ?>
					</div>
				</div>
				<div class="span8">
					<div id="container"></div>
					<div class="inspection_cat">
						<p>
                        
                                
                                <?php foreach($inspection as $list_inspection) : ?>
                                <?php if($list_inspection->inspection_type == 'PT'){
                                   $mrun=$this->db->query("select max(id) as maxi from record where hac='$id' and inspection_type='PT' GROUP BY inspection_type order by datetime desc")->row();    
                                ?>
                                    <span style='padding:6px'><a href='<?php echo base_url();?>report/main_report_list/report_penetrant_list/<?php echo $mrun->maxi;?>'><span><i class='icon-wear icon-inspection-bottom'></i>Penetrant</span></a></span>
                                    
                                    
                                 <?php }elseif($list_inspection->inspection_type == 'VIB'){ 
                                  $mrun=$this->db->query("select max(id) as maxi from record where hac='$id' and inspection_type='VIB' GROUP BY inspection_type order by datetime desc")->row();   
                                 ?>
                                    <span style='padding:6px'><a href='<?php echo base_url();?>report/main_report_list/report_vibration_list/<?php echo $mrun->maxi;?>'><span><i class='icon-vibration icon-inspection-bottom'></i>Vibration</span></a></span>
                                    
                                    
                                <?php }elseif($list_inspection->inspection_type == 'UT'){ 
                                $mrun=$this->db->query("select max(id) as maxi from record where hac='$id' and inspection_type='UT' GROUP BY inspection_type order by datetime desc")->row();   
                                ?>
                                    <span style='padding:6px'><a href='<?php echo base_url();?>report/main_report_list/report_ultrasonic_list/<?php echo $mrun->maxi;?>'><span><i class='icon-ultrasonic icon-inspection-bottom'></i>Ultrasonic</span></a></span>
                                    
                                    
                                <?php }elseif($list_inspection->inspection_type == 'THERMO'){ 
                                    $mrun=$this->db->query("select max(id) as maxi from record where hac='$id' and inspection_type='THERMO' GROUP BY inspection_type order by datetime desc")->row();   
                                ?>
                                    <span style='padding:6px'><a href='<?php echo base_url();?>report/main_report_list/report_thermo_list/<?php echo $mrun->maxi;?>'><span><i class='icon-thermo icon-inspection-bottom'></i>Thermography</span></a></span>
                                    
                                    
                                <?php }elseif($list_inspection->inspection_type == 'OA'){ 
                                    $mrun=$this->db->query("select max(id) as maxi from record where hac='$id' and inspection_type='OA' GROUP BY inspection_type order by datetime desc")->row();   
                                ?>
                                    <span style='padding:6px'><a href='<?php echo base_url();?>report/main_report_list/report_oil_analysis_list/<?php echo $mrun->maxi;?>'><span><i class='icon-oil icon-inspection-bottom'></i>Oil Analysis</span></a></span>
                                    
                                    
                                <?php }elseif($list_inspection->inspection_type == 'THICK_GENERAL'){ 
                                    $mrun=$this->db->query("select max(id) as maxi from record where hac='$id' and inspection_type='THICK_GENERAL' GROUP BY inspection_type order by datetime desc")->row();   
                                ?>
                                    <span style='padding:6px'><a href='<?php echo base_url();?>report/main_report_list/report_thickness_general_list/<?php echo $mrun->maxi;?>'><span><i class='icon-thickness icon-inspection-bottom'></i>Thickness GENERAL</span></a></span>
                                    
                                    
                               <?php }elseif($list_inspection->inspection_type == 'THICK_KILN'){
                                    $mrun=$this->db->query("select max(id) as maxi from record where hac='$id' and inspection_type='THICK_KILN' GROUP BY inspection_type order by datetime desc")->row();   
                                ?>
                                    <span style='padding:6px'><a href='<?php echo base_url();?>report/main_report_list/report_thickness_kiln_list/<?php echo $mrun->maxi?>'><span><i class='icon-kiln icon-inspection-bottom'></i>Thickness KILN</span></a></span>
                                    
                                    
                                <?php }elseif($list_inspection->inspection_type == 'THICK_STACK'){ 
                                    $mrun=$this->db->query("select max(id) as maxi from record where hac='$id' and inspection_type='THICK_STACK' GROUP BY inspection_type order by datetime desc")->row();   
                                ?>
                                   <span style='padding:6px'><a href='<?php echo base_url();?>report/main_report_list/report_thickness_stack_list/<?php echo $mrun->maxi;?>'><span><i class='icon-stack icon-inspection-bottom'></i>Thickness STACK</span></a></span>
                                    
                                    
                                <?php }elseif($list_inspection->inspection_type == 'MCSA'){ 
                                    $mrun=$this->db->query("select max(id) as maxi from record where hac='$id' and inspection_type='MCSA' GROUP BY inspection_type order by datetime desc")->row();   
                                ?>
                                    <span style='padding:6px'><a href='<?php echo base_url();?>report/main_report_list/report_mcsa_list/<?php echo $mrun->maxi;?>'><span><i class='icon-mcsa icon-inspection-bottom'></i>MCSA</span></a></span>
                                
                                <?php }elseif($list_inspection->inspection_type == 'MCA'){ 
                                    $mrun=$this->db->query("select max(id) as maxi from record where hac='$id' and inspection_type='MCA' GROUP BY inspection_type order by datetime desc")->row();   
                                ?>
                                    <span style='padding:6px'><a href='<?php echo base_url();?>report/main_report_list/report_mca_list/<?php echo $mrun->maxi;?>'><span><i class='icon-mca icon-inspection-bottom'></i>MCA</span></a></span>
                                    
                                    
                                <?php }elseif($list_inspection->inspection_type == 'IR'){ 
                                    $mrun=$this->db->query("select max(id) as maxi from record where hac='$id' and inspection_type='IR' GROUP BY inspection_type order by datetime desc")->row();   
                                ?>
                                    <span style='padding:6px'><a href='<?php echo base_url();?>report/main_report_list/report_inspection_list/<?php echo $mrun->maxi;?>'><span><i class='icon-inspection icon-inspection-bottom'></i>Running Inspection</span></a></span>
                                    
                                    
                                <?php }elseif($list_inspection->inspection_type == 'RUN'){ 
                                    $mrun=$this->db->query("select max(inspection_id) as maxi from record where hac='$id' and inspection_type='RUN' GROUP BY inspection_type order by datetime desc")->row();
                                    ?>
                                    
                                    <span style='padding:6px'><a href='<?php echo base_url();?>report/main_report_list/report_runningx_list/<?php echo $mrun->maxi;?>'><span><i class='icon-running icon-inspection-bottom'></i>Running Inspection</span></a></span>
                                    
                                    
                                <?php }elseif($list_inspection->inspection_type == 'STOP'){ 
                                      $mstop=$this->db->query("select max(inspection_id) as maxi from record where hac='$id' and inspection_type='STOP' GROUP BY inspection_type order by datetime desc")->row();
                                    ?>
                                    
                                    <span style='padding:6px'><a href='<?php echo base_url();?>report/main_report_list/report_stopxs_list/<?php echo $mstop->maxi;?>'><span><i class='icon-stopin icon-inspection-bottom'></i>Stop Inspection</span></a></span>
                                    
                                    
                                <?php }elseif($list_inspection->inspection_type == 'OTHERS'){ 
                                    $mrun=$this->db->query("select max(id) as maxi from record where hac='$id' and inspection_type='OTHERS' GROUP BY inspection_type order by datetime desc")->row();   
                                ?>
                                    <span style='padding:6px'><a href='<?php echo base_url();?>report/main_report_list/report_others_list/<?php echo $mrun->maxi;?>'><span><i class='icon-inspection icon-inspection-bottom'></i>Others Inspection</span></a></span>
                                    
                                <?php }elseif($list_inspection->inspection_type == 'WEAR'){ 
                                      $mstop=$this->db->query("select max(inspection_id) as maxi from record where hac='$id' and inspection_type='WEAR' GROUP BY inspection_type order by datetime desc")->row();
                                    ?>
                                    
                                    <span style='padding:6px'><a href='<?php echo base_url();?>report/main_report_list/report_wearx_list/<?php echo $mstop->maxi;?>'><span><i class='icon-wear icon-inspection-bottom'></i>Wear Inspection</span></a></span>
                                        
                                <?php } ?>
    							         <?php endforeach; ?>
						</p>
					</div>
				</div>
			</div>
			<div class="row-fluid">
                            <div class="span12">
                                <div class="spacer3"></div>
                                <div class="sparepart">
                                    <h3>Equipment History</h3>
                                        <div class="well">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>DATE</th>
                                                        <th>INSPECTION TYPE</th>
                                                        <th>SEVERITY</th>
                                                        <th>REPORTED BY</th>
                                                        <th style="text-align: center;">OTHER</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php 
                                                if(count($history)==""){
                                                    echo"<tr><td colspan='6' style='text-align:center;'>Data Not Found</td></tr>";
                                                }
                                                $offset = $this->uri->segment(5);
                                                $id= 1; foreach($history as $row){ 
                                                if($row->inspection_type=="RUN"){
                                                    $skt="Running Inspection";
                                                    $link = "report_runningx_list/".$row->inspection_id;
                                                }elseif($row->inspection_type=="STOP"){
                                                    $skt="Stop Inspection";
                                                    $link = "report_stopxs_list/".$row->inspection_id;
                                                }elseif($row->inspection_type=="WEAR"){
                                                    $skt="Wear Inspection";
                                                    $link = "report_wearx_list/".$row->inspection_id;
                                                }elseif($row->inspection_type=="VIB"){
                                                    $skt="Vibration Analysis";
                                                    $link = "report_vibration_list/".$row->id;
                                                }elseif($row->inspection_type=="OA"){
                                                    $skt="Oil Analysis";
                                                    $link = "report_oil_analysis_list/".$row->id;
                                                }elseif($row->inspection_type=="UT"){
                                                    $skt="Ultrasonic Test";
                                                    $link = "report_ultrasonic_list/".$row->id;
                                                }elseif($row->inspection_type=="PT"){
                                                    $skt="Penetrant Test";
                                                    $link = "report_penetrant_list/".$row->id;
                                                }elseif($row->inspection_type=="THICK_GENERAL"){
                                                    $skt="Thicknes General";
                                                    $link = "report_thickness_general_list/".$row->id;
                                                }elseif($row->inspection_type=="THICK_KILN"){
                                                    $skt="Thickness Kiln";
                                                    $link = "report_thickness_kiln_list/".$row->id;
                                                }elseif($row->inspection_type=="THICK_STACK"){
                                                    $skt="Thickness Stack";
                                                    $link = "report_thickness_stack_list/".$row->id;
                                                }elseif($row->inspection_type=="THERMO"){
                                                    $skt="Thermography";
                                                    $link = "report_thermo_list/".$row->id;
                                                }elseif($row->inspection_type=="MCA"){
                                                    $skt="MCA Inspection";
                                                    $link = "report_mca_list/".$row->id;
                                                }elseif($row->inspection_type=="MCSA"){
                                                    $skt="MCSA Inspection";
                                                    $link = "report_mcsa_list/".$row->id;
                                                }elseif($row->inspection_type=="IR"){
                                                    $skt="Inspection Report";
                                                    $link = "report_inspection_list/".$row->id;
                                                }elseif($row->inspection_type=="OTHERS"){
                                                    $skt="Others Inspection";
                                                    $link = "report_others_list/".$row->id;
                                                }    
                                                ?>
                                                    <tr>
                                                        <td><?php echo $row->datetime; ?></td>
                                                        <td><?php echo $skt; ?></td>
                                                        <td><?php if($row->level == '0'){ echo "Normal";}elseif($row->level == '1'){echo "Waning";}elseif($row->level == '2'){echo "Danger";}  ?></td>
                                                        <td><?php echo $row->nama; ?></td>
                                                         <td style="text-align: center;"><a href="<?=base_url();?>report/main_report_list/<?=$link;?>">View</a></td>
                                                     </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="pagination"><?=$halaman;?></div>
                                    </div>
				</div>
			</div>
		<div class="span12">
	<div class="spacer3"></div>
 <h3>Comment</h3>
<div class="row-fluid">
	<div class="span4">
		<div class="well">
			<div class="form-inline">
              	<?php if($this->session->userdata('access_login') != TRUE) { ?>
                    <?php echo "Please Login , Before give your comment"; ?>
						<?php } else { ?>
                        	<?php echo form_open('report/main_report/hac_comment_type/'); ?>
                                    <div class="control-group">
                                        <label class="control-label">Name</label>
                                        <div class="controls">	
                                            <input class="span9" type="text" name="nama"  value="<?=$detail_user->nama;?>" readonly="readonly"/>
                                            <input type="hidden" name="inspection_id" value="<?php echo $this->uri->segment(4);?>"/><input type="hidden" name="id_users" value="<?php echo $detail_user->id;?>"/>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Email</label>
                                        <div class="controls">	
                                            <input class="span9" type="email" name="email" value="<?=$detail_user->email;?>" readonly="readonly"/>
                                        </div>	
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Comment</label>
                                        <div class="controls">	
                                            <textarea class="span12" name="comment" rows="3" style="max-width: 275px;min-width: 275px;max-height: 100px;min-height: 100px;"></textarea>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label"></label>
                                        <div class="controls">	
                                            <button type="submit" class="btn">Send</button>
                                        </div>
                                    </div>
                                	<?php } ?>
     	                   	<?php echo form_close() ?>
                        </div>
                </div>
            </div>
					<div class="span8">
						<div class="well">
                        <?php foreach($comment as $row) : ?>
                        
                            <ul class="media-list">
								<li class="media">
                                
									<div class="media">
										<a class="pull-left" href="#">
											<img class="media-object" src="<?php echo base_url();?>media/images/<?=$row->photo;?>" alt="" />
										</a>
										<div class="media-body">
											<h4 class="media-heading"><?php echo $row->nama; ?></h4>
											<div><?php echo $row->comment;?></div>
											<div class="spacer3"></div>
										</div>
									</div>
								</li>
							</ul>
                            <?php endforeach; ?>
                            <div align="center">
                                    <?php if($comment == NULL){
                                    
                                    echo "<span style='padding:6px'>Not Comment</span>";
                                    
                                    
                                }?>
                                </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php 
$this->load->view('includes/footer.php');
?>		

<script src="<?=base_url()?>application/views/assets/report/js/highcharts.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/modules/exporting.js"></script>
<script type="text/javascript">
$(function () {
	$('#container').highcharts({
		title: {
			text: 'Severity Level Trend',
			x: -20 //center
		},
		subtitle: {
			text: '',
			x: -20
		},
		xAxis: {
			categories: [<?php foreach($severity_chart as $severity_chart_row) :?>'<?php echo date("d-m-Y", strtotime($severity_chart_row->datetime))?>',<?php endforeach;?>]
		},
		yAxis: {
			title: {
				text: 'Severity Level'
			},
             tickInterval: 1,
				  min: 0,
				  max: 3
            
		}
        ,
		tooltip: {
			valueSuffix: ' '
		},
		legend: {
			layout: 'horizontal',
			align: 'center',
			verticalAlign: 'bottom',
			borderWidth: 0
		},
		series: [{
			name: 'HAC',
              
			data: [<?php foreach($severity_chart as $severity_chart_row) :?>{y:<?php echo $severity_chart_row->severity;?>,color:'<?php $rian = $severity_chart_row->severity;
                                                        
                                                        if($rian =='0'){
                                                            
                                                            echo "#2e9612";
                                                            
                                                        }elseif($rian =='1'){
                                                            
                                                            echo "#f8fb00";
                                                        }
                                                        elseif($rian =='2'){
                                                            
                                                            echo "#e40000";
                                                        }
                                                        
                                                        
                                                        ?>'},<?php endforeach;?> ]
            
		},
        {
            name: 'Normal',
            data: [],
            marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/green.png)'
            }
        },
        {
            name: 'Warning',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/yellow.png)'
            }
        },
        {
            name: 'Danger',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/red.png)'
            }
        }]
	});
});


</script>