<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Header Report Area</title>
<link href="style.css" rel="stylesheet" />
<link href="./css/bootstrap.css" rel="stylesheet"/>
<link href="./css/bootstrap.min.css" rel="stylesheet"/>
<script type="text/javascript" src="./js/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript" src="./js/bootstrap.js"></script>
</head>

<body>
	<div id="main">
		<div id="content">
			<div class="inner">
				<div class="row-fluid">
					<div class="span12 header-map-detil">
						<div class="span1 status-sever">
							<div class="red"></div>
						</div>
						<div class="span2 status-haccode">
							<h2>491-AC1</h2>
						</div>
						<div class="span3 inspection">
							<span class="icon-fire icon-inspection-top" style="float:left;"></span><p>&nbsp;Thermography</p>
						</div>
						<div class="span4">
							<div id="pic-container">
								<div class="first">
									<img src="https://s3.amazonaws.com/uifaces/faces/twitter/kolage/128.jpg" class="thumb">
									<div class="name">Martono</div>
									<div class="nip">ID: 2948573647</div>
								</div>
								<div class="second">
									<img src="https://s3.amazonaws.com/uifaces/faces/twitter/terryxlife/128.jpg" class="thumb">
									<div class="name">Danang Suyatmo</div>
									<div class="nip">ID: 2948573690</div>
								</div>
							</div>
						</div>
						<div>
							<div class="jump-btn">
								<div class="btn-group">
									<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Other Reports&nbsp;&nbsp;<span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li style="text-align:center"><a href="#">Thermography</a></li>
										<li style="text-align:center"><a href="#">Stop Inspection</a></li>
										<li style="text-align:center"><a href="#">Inspection Report</a></li>
										<li style="text-align:center"><a href="#">Vibration Analysis</a></li>
										<li style="text-align:center"><a href="#">Kiln Thickness</a></li>
										<li style="text-align:center"><a href="#">Stack Thickness</a></li>
									</ul>
								</div>
							</div>
							<div class="pull-right back-btn">
								<a href="#" class="btn"><i class="icon-chevron-left"></i>Back</a>
							</div>
						</div>
					</div>
				</div>
					
					
					<div class="row-fluid">
						<div class="spacer"></div>
					</div>
					
					
				<div class="row-fluid">
					<div class="span12 header-map-detil">
						<div class="span1 status-sever">
							<div class="yellow"></div>
						</div>
						<div class="span2 status-haccode">
							<h2>491-AC5</h2>
						</div>
						<div class="span3 inspection">
							<span class="icon-blocked icon-inspection-top" style="float:left;"></span><p>&nbsp;Stop Inspection</p>
						</div>
						<div class="span4">
							<div id="pic-container">
								<div class="first">
									<img src="https://s3.amazonaws.com/uifaces/faces/twitter/divya/128.jpg" class="thumb">
									<div class="name">Ratnasari</div>
									<div class="nip">ID: 2948573632</div>
								</div>
								<div class="second">
									<img src="https://s3.amazonaws.com/uifaces/faces/twitter/gt/128.jpg" class="thumb">
									<div class="name">Budi Saputri</div>
									<div class="nip">ID: 2948573691</div>
								</div>
							</div>
						</div>
						<div>
							<div class="jump-btn">
								<div class="btn-group">
									<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Other Reports&nbsp;&nbsp;<span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li style="text-align:center"><a href="#">Thermography</a></li>
										<li style="text-align:center"><a href="#">Stop Inspection</a></li>
										<li style="text-align:center"><a href="#">Inspection Report</a></li>
										<li style="text-align:center"><a href="#">Vibration Analysis</a></li>
										<li style="text-align:center"><a href="#">Kiln Thickness</a></li>
										<li style="text-align:center"><a href="#">Stack Thickness</a></li>
									</ul>
								</div>
							</div>
							<div class="pull-right back-btn">
								<a href="#" class="btn"><i class="icon-chevron-left"></i>Back</a>
							</div>
						</div>
					</div>
				</div>
				
					<div class="row-fluid">
						<div class="spacer"></div>
					</div>
					
					
				<div class="row-fluid">
					<div class="span12 header-map-detil">
						<div class="span1 status-sever">
							<div class="green"></div>
						</div>
						<div class="span2 status-haccode">
							<h2>491-AC8</h2>
						</div>
						<div class="span3 inspection">
							<span class="icon-copy icon-inspection-top" style="float:left;"></span><p>&nbsp;Inspection Report</p>
						</div>
						<div class="span4">
							<div id="pic-container">
								<div class="first">
									<img src="https://s3.amazonaws.com/uifaces/faces/twitter/BillSKenney/128.jpg" class="thumb">
									<div class="name">Yantoasni</div>
									<div class="nip">ID: 2948573652</div>
								</div>
								<div class="second">
									<img src="https://s3.amazonaws.com/uifaces/faces/twitter/ruzinav/128.jpg" class="thumb">
									<div class="name">Ruth Sanjaya</div>
									<div class="nip">ID: 2948573677</div>
								</div>
							</div>
						</div>
						<div>
							<div class="jump-btn">
								<div class="btn-group">
									<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Other Reports&nbsp;&nbsp;<span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li style="text-align:center"><a href="#">Thermography</a></li>
										<li style="text-align:center"><a href="#">Stop Inspection</a></li>
										<li style="text-align:center"><a href="#">Inspection Report</a></li>
										<li style="text-align:center"><a href="#">Vibration Analysis</a></li>
										<li style="text-align:center"><a href="#">Kiln Thickness</a></li>
										<li style="text-align:center"><a href="#">Stack Thickness</a></li>
									</ul>
								</div>
							</div>
							<div class="pull-right back-btn">
								<a href="#" class="btn"><i class="icon-chevron-left"></i>Back</a>
							</div>
						</div>
					</div>
				</div>	

					<div class="row-fluid">
						<div class="spacer"></div>
					</div>
					
					
				<div class="row-fluid">
					<div class="span12 header-map-detil">
						<div class="span1 status-sever">
							<div class="green"></div>
						</div>
						<div class="span2 status-haccode">
							<h2>491-AB5</h2>
						</div>
						<div class="span3 inspection">
							<span class="icon-lightning icon-inspection-top" style="float:left;"></span><p>&nbsp;Vibration Analysis</p>
						</div>
						<div class="span4">
							<div id="pic-container">
								<div class="first">
									<img src="https://s3.amazonaws.com/uifaces/faces/twitter/BrianPurkiss/128.jpg" class="thumb">
									<div class="name">Sudarmani</div>
									<div class="nip">ID: 2948573562</div>
								</div>
								<div class="second">
									<img src="https://s3.amazonaws.com/uifaces/faces/twitter/amanruzaini/128.jpg" class="thumb">
									<div class="name">Eka Pratiwi</div>
									<div class="nip">ID: 2948573990</div>
								</div>
							</div>
						</div>
						<div>
							<div class="jump-btn">
								<div class="btn-group">
									<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Other Reports&nbsp;&nbsp;<span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li style="text-align:center"><a href="#">Thermography</a></li>
										<li style="text-align:center"><a href="#">Stop Inspection</a></li>
										<li style="text-align:center"><a href="#">Inspection Report</a></li>
										<li style="text-align:center"><a href="#">Vibration Analysis</a></li>
										<li style="text-align:center"><a href="#">Kiln Thickness</a></li>
										<li style="text-align:center"><a href="#">Stack Thickness</a></li>
									</ul>
								</div>
							</div>
							<div class="pull-right back-btn">
								<a href="#" class="btn"><i class="icon-chevron-left"></i>Back</a>
							</div>
						</div>
					</div>
				</div>

					<div class="row-fluid">
						<div class="spacer"></div>
					</div>
					
					
				<div class="row-fluid">
					<div class="span12 header-map-detil">
						<div class="span1 status-sever">
							<div class="red"></div>
						</div>
						<div class="span2 status-haccode">
							<h2>491-AB1</h2>
						</div>
						<div class="span3 inspection">
							<span class="icon-bars icon-inspection-top" style="float:left;"></span><p>&nbsp;Kiln Thickness</p>
						</div>
						<div class="span4">
							<div id="pic-container">
								<div class="first">
									<img src="https://s3.amazonaws.com/uifaces/faces/twitter/teeragit/128.jpg" class="thumb">
									<div class="name">Yoseph Otto</div>
									<div class="nip">ID: 2948573564</div>
								</div>
								<div class="second">
									<img src="https://s3.amazonaws.com/uifaces/faces/twitter/ilya_pestov/128.jpg" class="thumb">
									<div class="name">Johan Hendra</div>
									<div class="nip">ID: 2948573991</div>
								</div>
							</div>
						</div>
						<div>
							<div class="jump-btn">
								<div class="btn-group">
									<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Other Reports&nbsp;&nbsp;<span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li style="text-align:center"><a href="#">Thermography</a></li>
										<li style="text-align:center"><a href="#">Stop Inspection</a></li>
										<li style="text-align:center"><a href="#">Inspection Report</a></li>
										<li style="text-align:center"><a href="#">Vibration Analysis</a></li>
										<li style="text-align:center"><a href="#">Kiln Thickness</a></li>
										<li style="text-align:center"><a href="#">Stack Thickness</a></li>
									</ul>
								</div>
							</div>
							<div class="pull-right back-btn">
								<a href="#" class="btn"><i class="icon-chevron-left"></i>Back</a>
							</div>
						</div>
					</div>
				</div>

					<div class="row-fluid">
						<div class="spacer"></div>
					</div>
					
					
				<div class="row-fluid">
					<div class="span12 header-map-detil">
						<div class="span1 status-sever">
							<div class="yellow"></div>
						</div>
						<div class="span2 status-haccode">
							<h2>491-AD4</h2>
						</div>
						<div class="span3 inspection">
							<span class="icon-stack icon-inspection-top" style="float:left;"></span><p>&nbsp;Stack Thickness</p>
						</div>
						<div class="span4">
							<div id="pic-container">
								<div class="first">
									<img src="https://s3.amazonaws.com/uifaces/faces/twitter/jennyshen/128.jpg" class="thumb">
									<div class="name">Lili Liliput</div>
									<div class="nip">ID: 2948573587</div>
								</div>
								<div class="second">
									<img src="https://s3.amazonaws.com/uifaces/faces/twitter/diesellaws/128.jpg" class="thumb">
									<div class="name">Jackie Permana</div>
									<div class="nip">ID: 2948573988</div>
								</div>
							</div>
						</div>
						<div>
							<div class="jump-btn">
								<div class="btn-group">
									<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Other Reports&nbsp;&nbsp;<span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li style="text-align:center"><a href="#">Thermography</a></li>
										<li style="text-align:center"><a href="#">Stop Inspection</a></li>
										<li style="text-align:center"><a href="#">Inspection Report</a></li>
										<li style="text-align:center"><a href="#">Vibration Analysis</a></li>
										<li style="text-align:center"><a href="#">Kiln Thickness</a></li>
										<li style="text-align:center"><a href="#">Stack Thickness</a></li>
									</ul>
								</div>
							</div>
							<div class="pull-right back-btn">
								<a href="#" class="btn"><i class="icon-chevron-left"></i>Back</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>