<?php
$this->load->view('includes/header.php');	
?>
<style>
    a.tt{
        position: relative;
        z-index: 24;
        text-decoration: none;
    }
    a.tt span{
        display: none;
    }
    a.tt:hover{
        z-index: 25;
    }
    a.tt:hover span.tooltipx{
        display: block;
        position: absolute;
        //top: 10px; left:0;
        //padding: 0 20px 50px 0;
        //width: 200px;
        margin-left: 54px;
        margin-top: -52px;
        color: #993300;
        text-align: center;
    }
</style>
	<div id="main">
		<div id="content">
			<div class="inner">
				<div class="row-fluid">
					<div class="span12 header-map-detil">
						<div class="span4 title-condition">
							<h4><?=$image->description;?></h4>
						</div>
						<div class="span4">
						<div id="pic-container">
                       
                            <?php foreach($top as $top_row) :?>
                                <div style="float: left;padding-left: 3px;margin-top: -10px;">	    
                                    <a href="#" class="tt"><img src="<?php echo base_url();?>media/images/<?php echo $top_row->photo;?>" width="50px" height="50px" style="border: 1px solid;"/>
                                    <span class="topx"></span>
                                    <span class="tooltipx">
                                        <img src="<?php echo base_url();?>media/images/<?php echo $top_row->photo;?>" width="150px" height="150px" style="border: 1px solid;"/>
                                        <div style="background-color: grey;font-weight: bolder;color: white;text-transform: capitalize;text-align: left;">&nbsp;NIP : <?php echo $top_row->nip;?></div>
                                        <div style="background-color: grey;font-weight: bolder;color: white;text-transform: capitalize;text-align: left;">&nbsp;Nama : <?php echo $top_row->nama;?></div>
                                    </span>
                                    <span class="bottomx"></span></a>
                                </div>	
                            <?php endforeach;?>
                    
						</div>
					</div>
						<div class="span2 pull-right back-btn">
							<div class="btn-group">
									<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Jump To Area&nbsp;&nbsp;<span class="caret"></span></a>
									<ul class="dropdown-menu pull-right">
                                                                            <li style="text-align:center"><a href="<?=base_url()?>report/main_report/area/2">Area 2</a></li>
                                                                            <li style="text-align:center"><a href="<?=base_url()?>report/main_report/area/3-1">Area 3-1</a></li>
                                                                            <li style="text-align:center"><a href="<?=base_url()?>report/main_report/area/3-2">Area 3-2</a></li>
                                                                            <li style="text-align:center"><a href="<?=base_url()?>report/main_report/area/4-1">Area 4-1</a></li>
                                                                            <li style="text-align:center"><a href="<?=base_url()?>report/main_report/area/4-2">Area 4-2</a></li>
                                                                            <li style="text-align:center"><a href="<?=base_url()?>report/main_report/area/5-1">Area 5-1</a></li>
                                                                            <li style="text-align:center"><a href="<?=base_url()?>report/main_report/area/5-2">Area 5-2</a></li>
                                                                            <li style="text-align:center"><a href="<?=base_url()?>report/main_report/area/6">Area 6</a></li>
                                                                            <li style="text-align:center"><a href="<?=base_url()?>report/main_report/area/7">Area 7</a></li>
                                                                            <li style="text-align:center"><a href="<?=base_url()?>report/main_report/area/D">Area D</a></li>
                                                                            <li style="text-align:center"><a href="<?=base_url()?>report/main_report/area/E">Area E</a></li>
                                                                            <li style="text-align:center"><a href="<?=base_url()?>report/main_report/area/J">Area J</a></li>
                                                                            <li style="text-align:center"><a href="<?=base_url()?>report/main_report/area/K-1">Area K-1</a></li>
                                                                            <li style="text-align:center"><a href="<?=base_url()?>report/main_report/area/K-2">Area K-2</a></li>
                                                                            <li style="text-align:center"><a href="<?=base_url()?>report/main_report/area/L-1">Area L-1</a></li>
                                                                            <li style="text-align:center"><a href="<?=base_url()?>report/main_report/area/L-2">Area L-2</a></li>
                                                                            <li style="text-align:center"><a href="<?=base_url()?>report/main_report/area/M">Area M</a></li>
                                                                            <li style="text-align:center"><a href="<?=base_url()?>report/main_report/area/X-1">Area X-1</a></li>
                                                                            <li style="text-align:center"><a href="<?=base_url()?>report/main_report/area/X-2">Area X-2</a></li>
                                                                            
                                                                            
									</ul>
								</div>
						</div>
					</div>
				</div>
					
					
				<div class="row-fluid">
					<div class="spacer2"></div>
				</div>
					
					
				<div class="row-fluid">
					<div class="span4 sparepart">
						<img src="<?=base_url()?>media/images/<?=$image->image_main_area;?>" width="100%"/>
						<h3>About This Area <?php echo $id?></h3>
						<p>Description</p>
                                                <p><?=$image->description;?></p>
					</div>
					<div class="span8">
                                            <div  style="width: 800px;">
							<h3>Overall Condition</h3>
                            <ul style="list-style:  none;">
                                <?php
                                    $sql2b=$this->db->query("select * from hac where id in (select id from hac where hac_code LIKE '$like')
                                                 and severity_level LIKE '%2%'")->result();
                                    foreach($sql2b as $sql2x){
                                    ?>
                                    <li style="width: 150px;border: solid 1px;border-radius:5px;margin: 5px;text-decoration: none;float: left;text-align: center;background-color: red;">
                                     <a style="font-weight: bolder;font-size: 15px;color: black;" href="<?=base_url()?>report/main_report/hac/<?php echo $sql2x->id;?>"><?php echo $sql2x->hac_code;?></a>
                                    </li>
                                    <?php 
                                    }
                                    $sql2bx=$this->db->query("select * from hac where id in (select id from hac where hac_code LIKE '$like')
                                                 and severity_level not like '%2%' and severity_level like '%1%'")->result();
                                    foreach($sql2bx as $sql2xx){
                                    ?>
                                    <li style="width: 150px;border: solid 1px;border-radius:5px;margin: 5px;text-decoration: none;float: left;text-align: center;background-color: yellow;">
                                     <a style="font-weight: bolder;font-size: 15px;color: black;" href="<?=base_url()?>report/main_report/hac/<?php echo $sql2xx->id;?>"><?php echo $sql2xx->hac_code;?></a>
                                    </li>
                                 <?php 
                                    }
                                    $sql2bc=$this->db->query("select * from hac where id in (select id from hac where hac_code LIKE '$like')
                                                 and severity_level not like '%1%' and severity_level not like '%2%'")->result();
                                    foreach($sql2bc as $sql2xc){
                                    ?>
                                    <li style="width: 150px;border: solid 1px;border-radius:5px;margin: 5px;text-decoration: none;float: left;text-align: center;background-color: green;">
                                     <a style="font-weight: bolder;font-size: 15px;color: black;" href="<?=base_url()?>report/main_report/hac/<?php echo $sql2xc->id;?>"><?php echo $sql2xc->hac_code;?></a>
                                    </li>
                                 <?php } ?>
                             </ul>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>

	
<?php 
$this->load->view('includes/footer.php');
?>	