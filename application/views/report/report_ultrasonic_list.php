<?php
	$this->load->view('includes/header.php');
?>
<?php
  error_reporting(0);
  $ultrasonic_image="";
?>
<style>
    a.tt{
        position: relative;
        z-index: 24;
        text-decoration: none;
    }
    a.tt span{
        display: none;
    }
    a.tt:hover{
        z-index: 25;
    }
    a.tt:hover span.tooltipx{
        display: block;
        position: absolute;
        //top: 10px; left:0;
        //padding: 0 20px 50px 0;
        //width: 200px;
        margin-left: 54px;
        margin-top: -52px;
        color: #993300;
        text-align: center;
    }
</style>
<div id="main">
	<div id="content">
		<div class="inner">
			<div class="row-fluid">
				<div class="span12 header-map-detil">
					<div class="span1 status-sever">
						<?php  if ($ultrasonic_main['severity_level'] == '0'){
                            echo "<div class='green'></div>";
                            }elseif ($ultrasonic_main['severity_level'] == '1'){
                            echo "<div class='yellow'></div>";
                            }elseif ($ultrasonic_main['severity_level'] == '2'){
                            echo "<div class='red'></div>";
                            }
                        ?>
					</div>
					<div class="span2 status-haccode">
						<h4><?php echo $hac_code;?></h4>
					</div>
					<div class="span4">
						<div id="pic-container">
                       
                            <?php foreach($top->result() as $top_row) :?>
                                <div style="float: left;padding-left: 3px;margin-top: -10px;">	    
                                    <a href="#" class="tt"><img src="<?php echo base_url();?>media/images/<?php echo $top_row->photo;?>" width="50px" height="50px" style="border: 1px solid;"/>
                                    <span class="topx"></span>
                                    <span class="tooltipx">
                                        <img src="<?php echo base_url();?>media/images/<?php echo $top_row->photo;?>" width="150px" height="150px" style="border: 1px solid;"/>
                                        <div style="background-color: grey;font-weight: bolder;color: white;text-transform: capitalize;text-align: left;">&nbsp;NIP : <?php echo $top_row->nip;?></div>
                                        <div style="background-color: grey;font-weight: bolder;color: white;text-transform: capitalize;text-align: left;">&nbsp;Nama : <?php echo $top_row->nama;?></div>
                                    </span>
                                    <span class="bottomx"></span></a>
                                </div>	
                            <?php endforeach;?>
                    
						</div>
					</div>
					<div class="span2 inspection">
						<span class="icon-ultrasonic icon-inspection-top"></span><p>&nbsp;Ultrasonic</p>
					</div>
					<div class="span3">
						<div class="jump-btn">
							<div class="btn-group">
								<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Other Reports&nbsp;&nbsp;<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li style="text-align:center"><a href="<?=base_url()?>engine/inspection_manager/list_report_running_inspection">Running Inspection</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>engine/inspection_manager/list_report_stop_inspection">Stop Inspection</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_vibration/index/publish">Vibration Analysis</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_lubricant/index/publish">Lubricant Logbook</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_oil/index/publish">Oil Analysis</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_ultrasonic/index/publish">Ultrasonic Test</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/list_penetrant/index/publish">Penetrant Test</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>holcim/report/list_thickness/general/publish">Thicknes Measurement</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_thermo/index/publish">Thermography</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_mca/index/publish">MCA</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_mcsa/index/publish">MCSA</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_inspection/index/publish">Inspection Report</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>engine/inspection_manager/list_report_wear_inspection">Wear Measurement</a></li>
                                                                        <li style="text-align:center"><a href="<?=base_url()?>report/list_others/index/publish">Other Report</a></li>
								</ul>
							</div>
						</div>
						<div class="pull-right back-btn">
                                                    <a href="<?=base_url();?>report/list_ultrasonic/index/publish"><button class="btn"><i class="icon-chevron-left"></i>Back</button></a>
						</div>
					</div>
				</div>
			</div>
		<div class="row-fluid">
			<div class="spacer2"></div>
				<div class="span12">
					<div class="pull-left">
					<?php if($prev_last->id == $id ) {?>
							
					<?php }else{?>
							<a href="<?php echo base_url()?>report/main_report_list/report_ultrasonic_list/<?php echo $prev->id; ?>" class="btn"><i class="icon-chevron-left"></i>Prev</a>
						<?php }?>
					</div>
				<div class="pull-right">
				<?php if($next_last->id == $id ) {?>
					
					<?php }else{?>
						<a href="<?php echo base_url()?>report/main_report_list/report_ultrasonic_list/<?php echo $next->id; ?>" class="btn">Next<i class="icon-chevron-right"></i></a>
					<?php }?>
				</div>
				</div>
                        <div class="span12"></div>
				<div class="span12">
					<div class="span6">
						<img src="<?=base_url()?>media/images/<?php echo $ultrasonic_main['upload_file'];?>" width="100%" style="height: 300px; margin: 0 auto"/>
					</div>
					<div class="span6">
						<div id="container"></div>
					</div>
				</div>
			</div>
			
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Remarks</h3>
						<div><?php echo $ultrasonic_main['remarks'];?></div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Recommendation</h3>
						<div><?php echo $ultrasonic_main['recomendation'];?></div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Report List</h3>
						<div class="well">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>DATE</th>
										<th>SEVERITY</th>
										<th>USER</th>
                                                                                <th style="text-align: center;">OTHER</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($ultrasonic_detail as $ultrasonic_detail_row) :?>
									<tr>
										<td><?php echo $ultrasonic_detail_row->datetime; ?></td>
										<td>
                                                                                    <?php 
                                                                                    if($ultrasonic_detail_row->severity_level=="0"){echo "Normal"; }elseif($ultrasonic_detail_row->severity_level=="1"){echo"Warning";}elseif($ultrasonic_detail_row->severity_level=="2"){echo"Danger";}
                                                                                    ?>
                                                                                </td>
                                                                                <td><?php echo $ultrasonic_detail_row->nama; ?></td>
                                                                                <td style="text-align: center;"><a href="<?=base_url();?>report/main_report_list/report_ultrasonic_list/<?=$ultrasonic_detail_row->idx;?>">View</a></td>
									</tr>
                                    <?php endforeach;?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</div>

<?php 
$this->load->view('includes/footer.php');
?>		

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/highcharts.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/modules/exporting.js"></script>	

<script type="text/javascript">
$(function () {
	$('#container').highcharts({
		title: {
			text: 'Severity Level Trend',
			x: -20 //center
		},
		subtitle: {
			text: '',
			x: -20
		},
		xAxis: {
			categories: [<?php sort($severity_chart); 
            foreach($severity_chart as $severity_chart_row) :?>'<?php echo date("d-m-Y", strtotime($severity_chart_row->datetime)) ?>',<?php endforeach;?>]
		},
		legend: {
			layout: 'horizontal',
			align: 'center',
			verticalAlign: 'bottom',
			borderWidth: 0
		},
		yAxis: {
			title: {
				text: 'Severity Level'
			},
             tickInterval: 1,
				  min: 0,
				  max: 3
            
		}
        ,
		tooltip: {
			valueSuffix: ' '
		},
		legend: {
			layout: 'horizontal',
			align: 'center',
			verticalAlign: 'bottom',
			borderWidth: 0
		},
		series: [{
			name: 'Hac',
                        
			data: [<?php 
            sort($severity_chart);
            foreach($severity_chart as $severity_chart_row) :?>{y:<?php echo $severity_chart_row->severity_level;?>,color:'<?php $rian = $severity_chart_row->severity_level;
                                                        
                                                        if($rian =='0'){
                                                            
                                                            echo "#2e9612";
                                                            
                                                        }elseif($rian =='1'){
                                                            
                                                            echo "#f8fb00";
                                                        }
                                                        elseif($rian =='2'){
                                                            
                                                            echo "#e40000";
                                                        }
                                                        
                                                        
                                                        ?>'},<?php endforeach;?>],
		},
        {
            name: 'Normal',
            data: [],
            marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/green.png)'
            }
        },
        {
            name: 'Warning',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/yellow.png)'
            }
        },
        {
            name: 'Danger',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/red.png)'
            }
        }]
	});
});

function Next(){

	$("#container").html("");
	
	var options = {
		chart: {
			renderTo: 'container',
		},
		credits: {
			enabled: false
		},
		title: {
			text: 'Severity Level Trend',
			x: -20
		},
		xAxis: {
			categories: [{}]
		},
        
        yAxis: {
			title: {
				text: 'Severity Level'
			},
             tickInterval: 1,
				  min: 0,
				  max: 3
            
		},
		tooltip: {
            formatter: function() {
                var s = '<b>'+ this.x +'</b>';
                
                $.each(this.points, function(i, point) {
                    s += '<br/>'+point.series.name+': '+point.y;
                });
                
                return s;
            },
            shared: true
        },
		series: [{},{
            name: 'Normal',
            data: [],
            marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/green.png)'
            }
        },
        {
            name: 'Warning',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/yellow.png)'
            }
        },
        {
            name: 'Danger',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/red.png)'
            }
        }]
	};
	
	$.ajax({
		url: "json_ultra_next",
		data: 'show=data_json',
		type:'post',
		dataType: "json",
		success: function(data){
			options.xAxis.categories = data.categories;
			options.series[0].name = 'Hac';
			options.series[0].data = data.data_json;
            
			var chart = new Highcharts.Chart(options);			
		}
	});
	
}

function Previous(){

	$("#container").html("");
	
	var options = {
		chart: {
			renderTo: 'container',
		},
		credits: {
			enabled: false
		},
		title: {
			text: 'Severity Level Trend',
			x: -20
		},
		xAxis: {
			categories: [{}]
		},
        yAxis: {
			title: {
				text: 'Severity Level'
			},
             tickInterval: 1,
				  min: 0,
				  max: 3
            
		},
		tooltip: {
            formatter: function() {
                var s = '<b>'+ this.x +'</b>';
                
                $.each(this.points, function(i, point) {
                    s += '<br/>'+point.series.name+': '+point.y;
                });
                
                return s;
            },
            shared: true
        },
		series: [{},{
            name: 'Normal',
            data: [],
            marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/green.png)'
            }
        },
        {
            name: 'Warning',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/yellow.png)'
            }
        },
        {
            name: 'Danger',
            data: [],
             marker: {
                symbol: 'url(<?php echo base_url();?>application/views/assets/images/red.png)'
            }
        }]
	};
	
	$.ajax({
		url: "json_ultra_prev",
		data: 'show=data_json',
		type:'post',
		dataType: "json",
		success: function(data){
			options.xAxis.categories = data.categories;
			options.series[0].name = 'Hac';
			options.series[0].data = data.data_json;
			var chart = new Highcharts.Chart(options);			
		}
	});
	
}


</script>