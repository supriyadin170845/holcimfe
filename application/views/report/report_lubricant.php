<?php
	$this->load->view('includes/header.php');
?>

<div id="main">
	<div id="content">
		<div class="inner">
			<div class="row-fluid">
				<div class="span12 header-map-detil">
					<div class="span1 status-sever">
						<div class="green"></div>
					</div>
					<div class="span2 status-haccode">
						<h2>491-AC8</h2>
					</div>
					<div class="span4">
						<div id="pic-container">
							<div class="first">
								<img src="https://s3.amazonaws.com/uifaces/faces/twitter/BillSKenney/128.jpg" class="thumb">
								<div class="name">Yantoasni</div>
								<div class="nip">ID: 2948573652</div>
							</div>
							<div class="second">
								<img src="https://s3.amazonaws.com/uifaces/faces/twitter/ruzinav/128.jpg" class="thumb">
								<div class="name">Ruth Sanjaya</div>
								<div class="nip">ID: 2948573677</div>
							</div>
						</div>
					</div>
					<div class="span2 inspection">
						<span class="icon-inspection icon-inspection-top"></span><p>&nbsp;Ultrasonic</p>
					</div>
					<div class="span3">
						<div class="jump-btn">
							<div class="btn-group">
								<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Other Reports&nbsp;&nbsp;<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thermo">Thermography</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_stop">Stop Inspection</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_inspection">Inspection Report</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_vibration">Vibration Analysis</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thickness_80">Kiln Thickness</a></li>
									<li style="text-align:center"><a href="<?=base_url()?>report/main_report/report_thickness_16">Stack Thickness</a></li>
								</ul>
							</div>
						</div>
						<div class="pull-right back-btn">
							<a href="<?=base_url()?>report/main_report/hac" class="btn"><i class="icon-chevron-left"></i>Back</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer2"></div>
			</div>
			<div class="row-fluid">
				<div class="span12" id="slider">
					<!-- Top part of the slider -->
					<div class="row-fluid">
						<div class="span7" id="carousel-bounding-box">
							<div class="carousel slide" id="myCarousel">
								<!-- Carousel items -->
								<div class="carousel-inner">
									<div class="active item" data-slide-number="0">
										<img src="<?=base_url()?>application/views/assets/img/1802_1.png">
									</div>
									<div class="item" data-slide-number="1">
										<img src="<?=base_url()?>application/views/assets/img/1802_2.png">
									</div>
									<div class="item" data-slide-number="2">
										<img src="<?=base_url()?>application/views/assets/img/bearing_1.png">
									</div>
									<div class="item" data-slide-number="3">
										<img src="<?=base_url()?>application/views/assets/img/bearing_2.png">
									</div>
									<div class="item" data-slide-number="4">
										<img src="<?=base_url()?>application/views/assets/img/1810_1.png">
									</div>
									<div class="item" data-slide-number="5">
										<img src="<?=base_url()?>application/views/assets/img/1810_2.png">
									</div>
								</div><!-- Carousel nav -->
								<a class="carousel-control left" data-slide="prev" href="#myCarousel">‹</a> <a class="carousel-control right" data-slide="next" href="#myCarousel">›</a>
							</div>
						</div>
						<div class="span5" id="carousel-text"></div>
						<div id="slide-content" style="display: none;">
							<div id="slide-content-0">
								<h2>1802: Bevel Gear Hub</h2>
								<p>Bottom View</p>
								<p class="sub-text">December 2 2013</p>
							</div>
							<div id="slide-content-1">
								<h2>1802: Bevel Gear Hub</h2>
								<p>Bottom View 2</p>
								<p class="sub-text">December 2 2013</p>
							</div>
							<div id="slide-content-2">
								<h2>Stationary Stopper Bearing</h2>
								<p>After Clean Up</p>
								<p class="sub-text">December 2 2013</p>
							</div>
							<div id="slide-content-3">
								<h2>Stationary Stopper Bearing</h2>
								<p>After Clean Up 2</p>
								<p class="sub-text">December 2 2013></p>
							</div>
							<div id="slide-content-4">
								<h2>1810: Bevel Gear</h2>
								<p>Grinding Marking</p>
								<p class="sub-text">December 2 2013</p>
							</div>
							<div id="slide-content-5">
								<h2>1810: Bevel Gear</h2>
								<p>Painting Abrated</p>
								<p class="sub-text">December 2 2013</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row hidden-phone" id="slider-thumbs">
					<div class="span12">
						<ul class="thumbnails">
							<li class="span2">
								<a class="thumbnail" id="carousel-selector-0"><img src="<?=base_url()?>application/views/assets/img/thumbnail/1802_1_150x90.png"></a>
							</li>

							<li class="span2">
								<a class="thumbnail" id="carousel-selector-1"><img src="<?=base_url()?>application/views/assets/img/thumbnail/1802_2_150x90.png"></a>
							</li>

							<li class="span2">
								<a class="thumbnail" id="carousel-selector-2"><img src="<?=base_url()?>application/views/assets/img/thumbnail/bearing_1_150x90.png"></a>
							</li>

							<li class="span2">
								<a class="thumbnail" id="carousel-selector-3"><img src="<?=base_url()?>application/views/assets/img/thumbnail/bearing_2_150x90.png"></a>
							</li>

							<li class="span2">
								<a class="thumbnail" id="carousel-selector-4"><img src="<?=base_url()?>application/views/assets/img/thumbnail/1810_1_150x90.png"></a>
							</li>

							<li class="span2">
								<a class="thumbnail" id="carousel-selector-5"><img src="<?=base_url()?>application/views/assets/img/thumbnail/1810_2_150x90.png"></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="sparepart">
						<h3>Remarks</h3>
						<div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse faucibus rutrum adipiscing. Donec sollicitudin ultricies dignissim. Aenean blandit, lacus vel aliquet bibendum, ante libero lacinia purus, quis dictum metus purus id ante. Nam rhoncus neque ut magna gravida, non faucibus tortor porttitor. Mauris ultrices ligula sit amet libero porttitor auctor. Donec gravida elementum posuere. Aliquam scelerisque quam sit amet lorem hendrerit porta. Duis bibendum dapibus orci. Vivamus in libero hendrerit, auctor velit quis, imperdiet odio. Duis hendrerit velit eget pretium feugiat. Quisque metus eros, dictum eget faucibus vel, tincidunt ac est. Sed dapibus pretium sollicitudin.</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Recommendation</h3>
						<div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse faucibus rutrum adipiscing. Donec sollicitudin ultricies dignissim. Aenean blandit, lacus vel aliquet bibendum, ante libero lacinia purus, quis dictum metus purus id ante. Nam rhoncus neque ut magna gravida, non faucibus tortor porttitor. Mauris ultrices ligula sit amet libero porttitor auctor. Donec gravida elementum posuere. Aliquam scelerisque quam sit amet lorem hendrerit porta. Duis bibendum dapibus orci. Vivamus in libero hendrerit, auctor velit quis, imperdiet odio. Duis hendrerit velit eget pretium feugiat. Quisque metus eros, dictum eget faucibus vel, tincidunt ac est. Sed dapibus pretium sollicitudin.</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="spacer3"></div>
					<div class="sparepart">
						<h3>Report List</h3>
						<div class="well">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>DATE</th>
										<th>FILE NAME</th>
										<th>NOTES</th>
										<th>OTHER</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>14/02/2014</td>
										<td>ipsumdolor-sitamet.pdf</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
									<tr>
										<td>13/02/2014</td>
										<td>dolorsit.pdf</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
									<tr>
										<td>11/02/2014</td>
										<td>ipsum.pdf</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
									<tr>
										<td>10/02/2014</td>
										<td>loremsit-amet.pdf</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
									<tr>
										<td>05/02/2014</td>
										<td>amet.pdf</td>
										<td>Aliquam et nulla orci. Nullam imperdiet ultricies est, at luctus diam gravida id.</td>
										<td>N/A</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer"></div>
			</div>
		</div>
	</div>
</div>

<?php 
$this->load->view('includes/footer.php');
?>		

<script src="<?=base_url()?>application/views/assets/report/js/highcharts.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/modules/exporting.js"></script>		