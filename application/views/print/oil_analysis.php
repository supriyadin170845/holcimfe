<style>
    @media print {
        .hiddenx{
            display: none;
        }
 }
</style>
<center>
    <div style='width: 210mm;height:297mm;border: 1px solid;'>
        <table style="width: 100%;padding: 0px;font-size: 12px;" cellpadding="0" cellspacing="0" border="1">
            <tr>
                <td rowspan="5" style="width: 180px;text-align: center;">
                    <img src="<?php echo base_url()?>application/views/assets/img/logo_header.png" height="90px" width="180px">
                </td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center;font-size: 18px;">CONDITION BASED MONITORING REPORT</td>
                <td style="width: 110px;">&nbsp;Form Version</td>
                <td style="width: 110px;">&nbsp;: 2.0</td>
            <tr>
                <td>&nbsp;Release Date</td>
                <td>&nbsp;: 01/01/2014</td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center;font-size: 23px;"><b>Oil Analysis</b></td>
                <td>&nbsp;Reported Date</td>
                <td>&nbsp;: <?=date("d/m/Y", strtotime(substr($list->sys_create_date,0,10)));?></td>
            </tr>
            <tr>
                <td>&nbsp;Reporterd By</td>
                <td>&nbsp;: <?=$list->nama;?></td>
            </tr>
        </table>
        <div style="border: 1px solid;">
        <table style="width: 100%;padding: 0px;font-size: 12px;margin-top: -2px;" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td style="width: 145px;">&nbsp;&nbsp;&nbsp;Sample Number</td>
                <td style="width: 300px;">: <?=$list->sample_number;?></td>
                <td style="width: 140px;">Lube Analyst Number</td>
                <td>: <?=$list->lube_analyst_number;?></td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;Equipment Ref ID</td>
                <td>: <?=$list->equipment_ref_id;?></td>
                <td>Component Ref ID</td>
                <td>: <?=$list->component_ref_id;?></td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;Equipment Description</td>
                <td>: <?=$list->equipment_description;?></td>
                <td>Component Description</td>
                <td>: <?=$list->component_description;?></td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;Site Name</td>
                <td>: <?=$list->site_name;?></td>
                <td>Lubricant Name</td>
                <td>: <?=$list->lubricant_name;?></td>
            </tr>
        </table>
        </div>
        <div style="color: black;height: 20px;background: #D39E00;margin-top: 1px;border: 1px solid red;font-size: 15px;font-weight: bolder;">
            RESULT
        </div>
        <div style="float: left;">
            <div style="text-align: left;margin-left: 2px;float: left;">
            <table style="width: 140px;padding: 0px;font-size: 10px;" cellpadding="0" cellspacing="0" border="1">
                <tr>
                    <td>Sample Number</td>
                </tr>
                <tr>
                    <td>Sample Condition</td>
                </tr>
                <tr>
                    <td>Sample Date</td>
                </tr>
                <tr>
                    <td>Equipment Life</td>
                </tr>
                <tr>
                    <td>Lubricant Life</td>
                </tr>
                <tr>
                    <td style="background-color: #93a1a1">Appearance Special</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Appearance</td>
                </tr>
                <tr>
                    <td style="background-color: #93a1a1">Viscosity 40°C</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Visc 40°C cSt</td>
                </tr>
                <tr>
                    <td style="background-color: #93a1a1">TAN (D 664)</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;TAN (D 664) mg KOH/g</td>
                </tr>
                <tr>
                    <td style="background-color: #93a1a1">Water Content (Aquatest)</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Water Content (Aquatest) %</td>
                </tr>
                <tr>
                    <td style="background-color: #93a1a1">Spectrometry (Oils)</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Iron (Fe) ppm</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Chromium (Cr) ppm</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Nickel (Ni) ppm</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Aluminium (Al) ppm</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Copper (Cu) ppm</td>
                </tr><tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Lead (Pb) ppm</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Tin (Sn) ppm</td>
                </tr><tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Silver (Ag) ppm</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Titanium (Ti) ppm</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Vanadium (V) ppm</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Silicon (Si) ppm</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Sodium (Na) ppm</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Potassium (K) ppm</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Molybdenum (Mo) ppm</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Boron (B) ppm</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Magnesium (Mg) ppm</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Calcium (Ca) ppm</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Barium (Ba) ppm</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Phosphorus (P) ppm</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Zinc (Zn) ppm</td>
                </tr>
                
            </table>
        </div>
            <?php
            $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$list->sys_create_date' order by sys_create_date desc limit 0,3")->result();
            $hitung=count($data_list);
            $u=0;
            foreach(array_reverse($data_list) as $datax){
            $u++;
            if($u==$hitung){
                $bg="background-color: silver;";
            }else{
                $bg="";
            }
            ?>
            <div style="text-align: left;margin-left: -2px;float: left;">
            <table style="padding: 0px;font-size: 10px;<?=$bg;?>" cellpadding="0" cellspacing="0" border="1">
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->sample_number;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->sample_condition;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->sample_date;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->equipment_life;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->lubricant_life;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->appearance;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->viscosity;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->tan;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->water_content;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->iron;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->chromium;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->nickel;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->aluminium;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->copper;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->lead;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->tin;?></td>
                </tr><tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->silver;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->titanium;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->vanadium;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->silicon;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->sodium;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->potassium;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->molybdenum;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->boron;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->magnesium;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->calcium;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->barium;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->phosphorus;?></td>
                </tr>
                <tr>
                    <td align="center" style="padding-left: 2px;padding-right: 2px;"><?=$datax->zinc;?></td>
                </tr>
                
            </table>
        </div>
            <?php } ?>
        </div>
        <div id="container" style="width: 450px;height: 250px;float: right;"></div>
        <div id="container2" style="width: 450px;height: 250px;float: right;"></div>
        <div id="container3" style="width: 450px;height: 250px;float: right;"></div>
        <div style="color: black;height: 20px;background: #D39E00;margin-top: 1px;border: 1px solid red;font-size: 15px;font-weight: bolder;float: left;width: 40%;">
            Recomendation
        </div>
        <div style="float: left;width: 40%;text-align: justify;padding-left: 2px;"><?=$list->recomendation;?></div>
        <div style="color: black;height: 20px;background: #D39E00;margin-top: 1px;border: 1px solid red;font-size: 15px;font-weight: bolder;float: left;width: 100%;">
            Remarks
        </div>
        <div style="float: left;width: 100%;text-align: justify;padding-left: 2px;">
            <?=$list->remarks;?>
        </div>
    </div>
    <div style="padding-top: 10px;">
        <button class="hiddenx" onclick="window.print()">Print</button> <button class="hiddenx" style="display: <?=$display;?>;" onclick="window.history.back()">Back</button> <a href="<?=base_url();?>media/pdf/<?=$list->upload_file;?>" target="_blank"><button class="hiddenx">View PDF</button></a>
    </div>
</center>
<script type="text/javascript" src="<?php echo base_url() ?>application/views/assets/js/jquery-1.9.0.min.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/highcharts.js"></script>
<script src="<?=base_url()?>application/views/assets/report/js/modules/exporting.js"></script>	
<script type="text/javascript">
$(function () {
    $('#container').highcharts({
      title: {
            text: 'Oil Properties'
        },
        subtitle: {
            text: 'Oil Analysis'
        },
        xAxis: {
            categories: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$list->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo"'".date("d/m/Y",  strtotime(substr($datax->sys_create_date,0,10)))."',";
                }
                ?>
            ]
        },
        yAxis: {
            min: 0,
            max: 350,
            title: {
                text: 'Value'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        series: [{
            name: 'Visc 40°C cSt',
            data: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$list->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo $datax->viscosity.",";
                }
                ?>
            ]
        }, {
            name: 'TAN (D 664) mg KOH/g',
            data: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$list->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo $datax->tan.",";
                }
                ?>
            ]
        }]
    });
    $('#container2').highcharts({
        title: {
            text: 'Wear'
        },
        subtitle: {
            text: 'Oil Analysis'
        },
        xAxis: {
            categories: [
            <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$list->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo"'".date("d/m/Y",  strtotime(substr($datax->sys_create_date,0,10)))."',";
                }
                ?>
            ]
        },
        yAxis: {
            title: {
                text: 'Value'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        series: [{
            name: 'Aluminium (Al) ppm',
            data: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$list->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo $datax->aluminium.",";
                }
                ?>
            ]
        }, {
            name: 'Copper (Cu) ppm',
            data: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$list->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo $datax->copper.",";
                }
                ?>
            ]
        }, {
            name: 'Lead (Pb) ppm',
            data: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$list->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo $datax->lead.",";
                }
                ?>
            ]
        }, {
            name: 'Chromium (Cr) ppm',
            data: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$list->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo $datax->chromium.",";
                }
                ?>
            ]
        }, {
            name: 'Iron (Fe) ppm',
            data: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$list->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo $datax->iron.",";
                }
                ?>
            ]
        }]
    });
    $('#container3').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Pollution'
        },
        subtitle: {
            text: 'oil Analysis'
        },
        xAxis: {
            categories: [
            <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$list->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo"'".date("d/m/Y",  strtotime(substr($datax->sys_create_date,0,10)))."',";
                }
                ?>
            ]
        },
        yAxis: {
            title: {
                text: 'Value'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        series: [{
            name: 'Silicon (Si) ppm',
            data: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$list->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo $datax->silicon.",";
                }
                ?>
            ]
        }, {
            name: 'Sodium (Na) ppm',
            data: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$list->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo $datax->sodium.",";
                }
                ?>
            ]
        }, {
            name: 'Water Content (Aquatest) %',
            data: [
                <?php
                $data_list=$this->db->query("select * from record_oil_analysis WHERE sys_create_date<='$list->sys_create_date' order by sys_create_date desc limit 0,5")->result();
                foreach(array_reverse($data_list) as $datax){
                    echo $datax->water_content.",";
                }
                ?>
            ]
        }]
    });
});
</script>

