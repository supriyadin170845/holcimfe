<center>
    <div style='width: 297mm;height:200mm;border: 1px solid;'>
        <table style="width: 100%;padding: 0px;font-size: 15px;" cellpadding="0" cellspacing="0" border="1">
            <tr>
                <td rowspan="5" style="width: 180px;text-align: center;">
                    <img src="<?php echo base_url()?>application/views/assets/img/logo_header.png" height="90px" width="150px">
                </td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center;font-size: 18px;">CONDITION BASED MONITORING REPORT</td>
                <td style="width: 110px;">&nbsp;Form Version</td>
                <td style="width: 110px;">&nbsp;: 2.0</td>
            <tr>
                <td>&nbsp;Release Date</td>
                <td>&nbsp;: 01/01/2014</td>
            </tr>

            <tr>
                <td rowspan="2" style="text-align: center;font-size: 21px;">STACK THICKNESS REPORT</td>
                <td>&nbsp;Reported Date</td>
                <td>&nbsp;: <?=date("d/m/Y",strtotime($list->datetime));?></td>
            </tr>
            <tr>
                <td>&nbsp;Reporterd By</td>
                <td>&nbsp;: <?=$list->nama_ins;?></td>
            </tr>
        </table>
        <table style="width: 100%;padding: 0px;font-size: 15px;" cellpadding="0" cellspacing="0" border="1">
            <tr>
                <td style="width: 330px;"><div style="width: 150px;float: left;">HAC</div>: <?=$list->hac_code;?></td>
                <td colspan="12">Test Object : <?=$list->test_object;?></td>
            </tr>
            <tr>
                <td><div style="width: 150px;float: left;">Model</div>: <?=$list->model;?></td>
                <td style="background-color: gray;"></td>
                <td style="text-align: center;">Operator</td>
                <td style="text-align: center;">Approved</td>
                <td colspan="2" rowspan="5" style="text-align: center;" width="50px;">
                    <img src="<?=base_url();?>application/views/assets/img/thickness_record_picture.jpg">
                </td>
            </tr>
            <tr>
                <td><div style="width: 150px;float: left;">Couplant</div>: <?=$list->couplant;?></td>
                <td>Name</td>
                <td style='text-align: center;'><?=$list->nama_ins;?></td>
                <td style='text-align: center;'><?=$list->nama_pub?></td>
                
            </tr>
            <tr>
                <td><div style="width: 150px;float: left;">Probe Type</div>: <?=$list->probe_type;?></td>
                <td>Sign.</td>
                <td style="text-align: center;"><img src="<?=base_url();?>media/images/<?=$list->signature_ins;?>" width="70" height="15"></td>
                <td style="text-align: center;"><img src="<?=base_url();?>media/images/<?=$list->signature_pub;?>" width="70" height="15"></td>
            
                
            </tr>
            <tr>
                <td><div style="width: 150px;float: left;">Frequency</div>: <?=$list->frequency;?> Mhz</td>
                <td colspan="3" rowspan="2">
                </td>
            </tr>
            <tr>
                <td><div style="width: 150px;float: left;">Thickness</div>: <?=$list->thickness;?> mm</td>
            </tr>
            <tr>
                <td colspan="12" style="text-align: center; font-weight: bolder;">PICTURE / SKETCH</td>
            </tr>
            <tr>
                <td colspan="12" style="text-align: center;">
                    <img src="<?=base_url();?>media/images/<?=$list->upload_file;?>" width="800px" height="250px">
                </td>
            </tr>
        </table>
        <table style="width: 100%;padding: 0px;font-size: 15px;" cellpadding="0" cellspacing="0" border="1">
            <tr align="center">
                    <td>MEASUREMENT POINT</td>
            </tr>
        </table>	
                <?php
                $pt1=explode(",", $list->point_1);
                $pt2=explode(",", $list->point_2);
                $pt3=explode(",", $list->point_3);
                $pt4=explode(",", $list->point_4);
                $pt5=explode(",", $list->point_5);
                $pt6=explode(",", $list->point_6);
                $pt7=explode(",", $list->point_7);
                $pt8=explode(",", $list->point_8);
                $pt9=explode(",", $list->point_9);
                $pt10=explode(",", $list->point_10);
                $pt11=explode(",", $list->point_11);
                $pt12=explode(",", $list->point_12);
                $pt13=explode(",", $list->point_13);
                $pt14=explode(",", $list->point_14);
                $pt15=explode(",", $list->point_15);
                $pt16=explode(",", $list->point_16);
                ?>
        <table style="width: 100%;padding: 0px;font-size: 11px;" cellpadding="0" cellspacing="0" border="1">
            <thead  bgcolor="#888888" border="1">
                <tr align="center">
                        <th>Angle</th><th>&nbsp;1</th><th>&nbsp;2</th><th>&nbsp;3</th><th>&nbsp;4</th><th>&nbsp;5</th><th>&nbsp;6</th><th>&nbsp;7</th><th>&nbsp;8</th><th>&nbsp;9</th><th>10</th><th>11</th><th>12</th><th>13</th><th>14</th><th>15</th><th>16</th>
                </tr>
            </thead>
            <tbody>
                <tr align="center">
                    <td>0&deg;</td><td><?=$pt1[0];?></td><td><?=$pt2[0];?></td><td><?=$pt3[0];?></td><td><?=$pt4[0];?></td><td><?=$pt5[0];?></td><td><?=$pt6[0];?></td><td><?=$pt7[0];?></td><td><?=$pt8[0];?></td><td><?=$pt9[0];?></td><td><?=$pt10[0];?></td><td><?=$pt11[0];?></td><td><?=$pt12[0];?></td><td><?=$pt13[0];?></td><td><?=$pt14[0];?></td><td><?=$pt15[0];?></td><td><?=$pt16[0];?></td>
                </tr>				
                <tr align="center">
                    <td>90&deg;</td><td><?=$pt1[1];?></td><td><?=$pt2[1];?></td><td><?=$pt3[1];?></td><td><?=$pt4[1];?></td><td><?=$pt5[1];?></td><td><?=$pt6[1];?></td><td><?=$pt7[1];?></td><td><?=$pt8[1];?></td><td><?=$pt9[1];?></td><td><?=$pt10[1];?></td><td><?=$pt11[1];?></td><td><?=$pt12[1];?></td><td><?=$pt13[1];?></td><td><?=$pt14[1];?></td><td><?=$pt15[1];?></td><td><?=$pt16[1];?></td>
                </tr>				
                <tr align="center">
                    <td>180&deg;</td><td><?=$pt1[2];?></td><td><?=$pt2[2];?></td><td><?=$pt3[2];?></td><td><?=$pt4[2];?></td><td><?=$pt5[2];?></td><td><?=$pt6[2];?></td><td><?=$pt7[2];?></td><td><?=$pt8[2];?></td><td><?=$pt9[2];?></td><td><?=$pt10[2];?></td><td><?=$pt11[2];?></td><td><?=$pt12[2];?></td><td><?=$pt13[2];?></td><td><?=$pt14[2];?></td><td><?=$pt15[2];?></td><td><?=$pt16[2];?></td>
                </tr>				
                <tr align="center">
                    <td>270&deg;</td><td><?=$pt1[3];?></td><td><?=$pt2[3];?></td><td><?=$pt3[3];?></td><td><?=$pt4[3];?></td><td><?=$pt5[3];?></td><td><?=$pt6[3];?></td><td><?=$pt7[3];?></td><td><?=$pt8[3];?></td><td><?=$pt9[3];?></td><td><?=$pt10[3];?></td><td><?=$pt11[3];?></td><td><?=$pt12[3];?></td><td><?=$pt13[3];?></td><td><?=$pt14[3];?></td><td><?=$pt15[3];?></td><td><?=$pt16[3];?></td>
                </tr>
            </tbody>
        </table>
        <div style="padding: 10px;text-align: left;">
            Remarks:
            <?=$list->remarks;?>
        </div>
    </div>
    <div style="margin-top:10px;text-align:center">
        <form action="<?php echo base_url();?>print_data/update_thickness/<?php echo $list->idx;?>" method="post">
	<div class="btn-group">
            <?php if($list->status != "publish"){
                    echo "<button type='submit' name='status' value='publish' class='dontprint'>Publish</button>
                        <input type='hidden' name='type' value='stack'>
                    <button type='submit' name='status' value='reject' class='dontprint'>Reject</button>";
            ?>
             </form>
            <?php
            }elseif ($list->status == "publish"){
                    echo "<input type='button' class='dontprint' onclick='window.print()' value='Print Layout'>";
            }
            ?>
            <input type="button" onClick="location.href='<?php echo base_url();?>record/thickness/stack'" class='dontprint' value='Back'>
	</div>
	</form>	
    </div>
</center>
<style type="text/css">
    @media print {
    body {-webkit-print-color-adjust: exact;}
    .dontprint{ display: none; }
    @page {size: landscape}
}
</style>