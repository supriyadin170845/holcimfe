<center>
    <div style='width: 210mm;height:297mm;border: 1px solid;'>
        <table style="width: 100%;padding: 0px;font-size: 15px;" cellpadding="0" cellspacing="0" border="1">
            <tr>
                <td rowspan="5" style="width: 180px;text-align: center;">
                    <img src="<?php echo base_url()?>application/views/assets/img/logo_header.png" height="90px" width="150px">
                </td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center;font-size: 18px;">CONDITION BASED MONITORING REPORT</td>
                <td style="width: 110px;">&nbsp;Form Version</td>
                <td style="width: 110px;">&nbsp;: 2.0</td>
            <tr>
                <td>&nbsp;Release Date</td>
                <td>&nbsp;: 01/01/2014</td>
            </tr>

            <tr>
                <td rowspan="2" style="text-align: center;font-size: 21px;">KILN THICKNESS REPORT</td>
                <td>&nbsp;Reported Date</td>
                <td>&nbsp;: <?=date("d/m/Y",strtotime($list->datetime));?></td>
            </tr>
            <tr>
                <td>&nbsp;Reporterd By</td>
                <td>&nbsp;: <?=$list->nama_ins;?></td>
            </tr>
        </table>
        <table style="width: 100%;padding: 0px;font-size: 15px;" cellpadding="0" cellspacing="0" border="1">
            <tr>
                <td style="width: 330px;"><div style="width: 150px;float: left;">HAC</div>: <?=$list->hac_code;?></td>
                <td colspan="12">Test Object : <?=$list->test_object;?></td>
            </tr>
            <tr>
                <td><div style="width: 150px;float: left;">Model</div>: <?=$list->model;?></td>
                <td style="background-color: gray;"></td>
                <td style="text-align: center;">Operator</td>
                <td style="text-align: center;">Approved</td>
                <td colspan="2" rowspan="5" style="text-align: center;" width="50px;">
                    <img src="<?=base_url();?>application/views/assets/img/thickness_record_picture.jpg">
                </td>
            </tr>
            <tr>
                <td><div style="width: 150px;float: left;">Couplant</div>: <?=$list->couplant;?></td>
                <td>Name</td>
                <td style='text-align: center;'><?=$list->nama_ins;?></td>
                <td style='text-align: center;'><?=$list->nama_pub?></td>
                
            </tr>
            <tr>
                <td><div style="width: 150px;float: left;">Probe Type</div>: <?=$list->probe_type;?></td>
                <td>Sign.</td>
                <td style="text-align: center;"><img src="<?=base_url();?>media/images/<?=$list->signature_ins;?>" width="70" height="15"></td>
                <td style="text-align: center;"><img src="<?=base_url();?>media/images/<?=$list->signature_pub;?>" width="70" height="15"></td>
            
                
            </tr>
            <tr>
                <td><div style="width: 150px;float: left;">Frequency</div>: <?=$list->frequency;?> Mhz</td>
                <td colspan="3" rowspan="2">
                </td>
            </tr>
            <tr>
                <td><div style="width: 150px;float: left;">Thickness</div>: <?=$list->thickness;?> mm</td>
            </tr>
            <tr>
                <td colspan="12" style="text-align: center; font-weight: bolder;">PICTURE / SKETCH</td>
            </tr>
            <tr>
                <td colspan="12" style="text-align: center;">
                    <img src="<?=base_url();?>media/images/<?=$list->upload_file;?>" width="780px" height="250px">
                </td>
            </tr>
        </table>
        <table style="width: 100%;padding: 0px;font-size: 15px;" cellpadding="0" cellspacing="0" border="1">
            <tr align="center">
                    <td>MEASUREMENT POINT</td>
            </tr>
        </table>	
                 <?php
                $pt1=explode(",", $list->point_1);
                $pt2=explode(",", $list->point_2);
                $pt3=explode(",", $list->point_3);
                $pt4=explode(",", $list->point_4);
                $pt5=explode(",", $list->point_5);
                $pt6=explode(",", $list->point_6);
                $pt7=explode(",", $list->point_7);
                $pt8=explode(",", $list->point_8);
                $pt9=explode(",", $list->point_9);
                $pt10=explode(",", $list->point_10);
                
                $pt11=explode(",", $list->point_11);
                $pt12=explode(",", $list->point_12);
                $pt13=explode(",", $list->point_13);
                $pt14=explode(",", $list->point_14);
                $pt15=explode(",", $list->point_15);
                $pt16=explode(",", $list->point_16);
                $pt17=explode(",", $list->point_17);
                $pt18=explode(",", $list->point_18);
                $pt19=explode(",", $list->point_19);
                $pt20=explode(",", $list->point_20);
                
                $pt21=explode(",", $list->point_21);
                $pt22=explode(",", $list->point_22);
                $pt23=explode(",", $list->point_23);
                $pt24=explode(",", $list->point_24);
                $pt25=explode(",", $list->point_25);
                $pt26=explode(",", $list->point_26);
                $pt27=explode(",", $list->point_27);
                $pt28=explode(",", $list->point_28);
                $pt29=explode(",", $list->point_29);
                $pt30=explode(",", $list->point_30);
                
                $pt31=explode(",", $list->point_31);
                $pt32=explode(",", $list->point_32);
                $pt33=explode(",", $list->point_33);
                $pt34=explode(",", $list->point_34);
                $pt35=explode(",", $list->point_35);
                $pt36=explode(",", $list->point_36);
                $pt37=explode(",", $list->point_37);
                $pt38=explode(",", $list->point_38);
                $pt39=explode(",", $list->point_39);
                $pt40=explode(",", $list->point_40);
                
                $pt41=explode(",", $list->point_41);
                $pt42=explode(",", $list->point_42);
                $pt43=explode(",", $list->point_43);
                $pt44=explode(",", $list->point_44);
                $pt45=explode(",", $list->point_45);
                $pt46=explode(",", $list->point_46);
                $pt47=explode(",", $list->point_47);
                $pt48=explode(",", $list->point_48);
                $pt49=explode(",", $list->point_49);
                $pt50=explode(",", $list->point_50);
                
                $pt51=explode(",", $list->point_51);
                $pt52=explode(",", $list->point_52);
                $pt53=explode(",", $list->point_53);
                $pt54=explode(",", $list->point_54);
                $pt55=explode(",", $list->point_55);
                $pt56=explode(",", $list->point_56);
                $pt57=explode(",", $list->point_57);
                $pt58=explode(",", $list->point_58);
                $pt59=explode(",", $list->point_59);
                $pt60=explode(",", $list->point_60);
                
                $pt61=explode(",", $list->point_61);
                $pt62=explode(",", $list->point_62);
                $pt63=explode(",", $list->point_63);
                $pt64=explode(",", $list->point_64);
                $pt65=explode(",", $list->point_65);
                $pt66=explode(",", $list->point_66);
                $pt67=explode(",", $list->point_67);
                $pt68=explode(",", $list->point_68);
                $pt69=explode(",", $list->point_69);
                $pt70=explode(",", $list->point_70);
                
                $pt71=explode(",", $list->point_71);
                $pt72=explode(",", $list->point_72);
                $pt73=explode(",", $list->point_73);
                $pt74=explode(",", $list->point_74);
                $pt75=explode(",", $list->point_75);
                $pt76=explode(",", $list->point_76);
                $pt77=explode(",", $list->point_77);
                $pt78=explode(",", $list->point_78);
                $pt79=explode(",", $list->point_79);
                $pt80=explode(",", $list->point_80);
                ?>
        <table style="width: 100%;padding: 0px;font-size: 9px;" cellpadding="0" cellspacing="0" border="1">
            <thead  bgcolor="#888888" border="1">
            <tr align="center">
                    <th>Angle</th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th><th>7</th><th>8</th><th>9</th><th>10</th><th>11</th><th>12</th><th>13</th><th>14</th><th>15</th><th>16</th>
            </tr>
            </thead>
            <tbody>
                <tr align="center">
                    <td>0&deg;</td><td><?=$pt1[0];?></td><td><?=$pt2[0];?></td><td><?=$pt3[0];?></td><td><?=$pt4[0];?></td><td><?=$pt5[0];?></td><td><?=$pt6[0];?></td><td><?=$pt7[0];?></td><td><?=$pt8[0];?></td><td><?=$pt9[0];?></td><td><?=$pt10[0];?></td><td><?=$pt11[0];?></td><td><?=$pt12[0];?></td><td><?=$pt13[0];?></td><td><?=$pt14[0];?></td><td><?=$pt15[0];?></td><td><?=$pt16[0];?></td>
                </tr>				
                <tr align="center">
                    <td>90&deg;</td><td><?=$pt1[1];?></td><td><?=$pt2[1];?></td><td><?=$pt3[1];?></td><td><?=$pt4[1];?></td><td><?=$pt5[1];?></td><td><?=$pt6[1];?></td><td><?=$pt7[1];?></td><td><?=$pt8[1];?></td><td><?=$pt9[1];?></td><td><?=$pt10[1];?></td><td><?=$pt11[1];?></td><td><?=$pt12[1];?></td><td><?=$pt13[1];?></td><td><?=$pt14[1];?></td><td><?=$pt15[1];?></td><td><?=$pt16[1];?></td>
                </tr>				
                <tr align="center">
                    <td>180&deg;</td><td><?=$pt1[2];?></td><td><?=$pt2[2];?></td><td><?=$pt3[2];?></td><td><?=$pt4[2];?></td><td><?=$pt5[2];?></td><td><?=$pt6[2];?></td><td><?=$pt7[2];?></td><td><?=$pt8[2];?></td><td><?=$pt9[2];?></td><td><?=$pt10[2];?></td><td><?=$pt11[2];?></td><td><?=$pt12[2];?></td><td><?=$pt13[2];?></td><td><?=$pt14[2];?></td><td><?=$pt15[2];?></td><td><?=$pt16[2];?></td>
                </tr>				
                <tr align="center">
                    <td>270&deg;</td><td><?=$pt1[3];?></td><td><?=$pt2[3];?></td><td><?=$pt3[3];?></td><td><?=$pt4[3];?></td><td><?=$pt5[3];?></td><td><?=$pt6[3];?></td><td><?=$pt7[3];?></td><td><?=$pt8[3];?></td><td><?=$pt9[3];?></td><td><?=$pt10[3];?></td><td><?=$pt11[3];?></td><td><?=$pt12[3];?></td><td><?=$pt13[3];?></td><td><?=$pt14[3];?></td><td><?=$pt15[3];?></td><td><?=$pt16[3];?></td>
                </tr>
            </tbody>
        </table>
        <table style="width: 100%;padding: 0px;font-size: 9px;" cellpadding="0" cellspacing="0" border="1">
            <thead  bgcolor="#888888" border="1">
            <tr align="center">
                    <th>Angle</th><th>17</th><th>18</th><th>19</th><th>20</th><th>21</th><th>22</th><th>23</th><th>24</th><th>25</th><th>26</th><th>27</th><th>28</th><th>29</th><th>30</th><th>31</th><th>32</th>
            </tr>
            </thead>
            <tbody>
                <tr align="center">
                    <td>0&deg;</td><td><?=$pt17[0];?></td><td><?=$pt18[0];?></td><td><?=$pt19[0];?></td><td><?=$pt20[0];?></td><td><?=$pt21[0];?></td><td><?=$pt22[0];?></td><td><?=$pt23[0];?></td><td><?=$pt24[0];?></td><td><?=$pt25[0];?></td><td><?=$pt26[0];?></td><td><?=$pt27[0];?></td><td><?=$pt28[0];?></td><td><?=$pt29[0];?></td><td><?=$pt30[0];?></td><td><?=$pt31[0];?></td><td><?=$pt32[0];?></td>
                </tr>				
                <tr align="center">
                    <td>90&deg;</td><td><?=$pt17[1];?></td><td><?=$pt18[1];?></td><td><?=$pt19[1];?></td><td><?=$pt20[1];?></td><td><?=$pt21[1];?></td><td><?=$pt22[1];?></td><td><?=$pt23[1];?></td><td><?=$pt24[1];?></td><td><?=$pt25[1];?></td><td><?=$pt26[1];?></td><td><?=$pt27[1];?></td><td><?=$pt28[1];?></td><td><?=$pt29[1];?></td><td><?=$pt30[1];?></td><td><?=$pt31[1];?></td><td><?=$pt32[1];?></td>
                </tr>				
                <tr align="center">
                    <td>180&deg;</td><td><?=$pt17[2];?></td><td><?=$pt18[2];?></td><td><?=$pt19[2];?></td><td><?=$pt20[2];?></td><td><?=$pt21[2];?></td><td><?=$pt22[2];?></td><td><?=$pt23[2];?></td><td><?=$pt24[2];?></td><td><?=$pt25[2];?></td><td><?=$pt26[2];?></td><td><?=$pt27[2];?></td><td><?=$pt28[2];?></td><td><?=$pt29[2];?></td><td><?=$pt30[2];?></td><td><?=$pt31[2];?></td><td><?=$pt32[2];?></td>
                </tr>				
                <tr align="center">
                    <td>270&deg;</td><td><?=$pt17[3];?></td><td><?=$pt18[3];?></td><td><?=$pt19[3];?></td><td><?=$pt20[3];?></td><td><?=$pt21[3];?></td><td><?=$pt22[3];?></td><td><?=$pt23[3];?></td><td><?=$pt24[3];?></td><td><?=$pt25[3];?></td><td><?=$pt26[3];?></td><td><?=$pt27[3];?></td><td><?=$pt28[3];?></td><td><?=$pt29[3];?></td><td><?=$pt30[3];?></td><td><?=$pt31[3];?></td><td><?=$pt32[3];?></td>
                </tr>
            </tbody>
        </table>
        <table style="width: 100%;padding: 0px;font-size: 9px;" cellpadding="0" cellspacing="0" border="1">
            <thead  bgcolor="#888888" border="1">
            <tr align="center">
                <th>Angle</th><th>&nbsp;1</th><th>&nbsp;2</th><th>&nbsp;3</th><th>&nbsp;4</th><th>&nbsp;5</th><th>&nbsp;6</th><th>&nbsp;7</th><th>&nbsp;8</th><th>&nbsp;9</th><th>10</th><th>11</th><th>12</th><th>13</th><th>14</th><th>15</th><th>16</th>
            </tr>
            </thead>
            <tbody>
                <tr align="center">
                    <td>0&deg;</td><td><?=$pt1[0];?></td><td><?=$pt2[0];?></td><td><?=$pt3[0];?></td><td><?=$pt4[0];?></td><td><?=$pt5[0];?></td><td><?=$pt6[0];?></td><td><?=$pt7[0];?></td><td><?=$pt8[0];?></td><td><?=$pt9[0];?></td><td><?=$pt10[0];?></td><td><?=$pt11[0];?></td><td><?=$pt12[0];?></td><td><?=$pt13[0];?></td><td><?=$pt14[0];?></td><td><?=$pt15[0];?></td><td><?=$pt16[0];?></td>
                </tr>				
                <tr align="center">
                    <td>90&deg;</td><td><?=$pt1[1];?></td><td><?=$pt2[1];?></td><td><?=$pt3[1];?></td><td><?=$pt4[1];?></td><td><?=$pt5[1];?></td><td><?=$pt6[1];?></td><td><?=$pt7[1];?></td><td><?=$pt8[1];?></td><td><?=$pt9[1];?></td><td><?=$pt10[1];?></td><td><?=$pt11[1];?></td><td><?=$pt12[1];?></td><td><?=$pt13[1];?></td><td><?=$pt14[1];?></td><td><?=$pt15[1];?></td><td><?=$pt16[1];?></td>
                </tr>				
                <tr align="center">
                    <td>180&deg;</td><td><?=$pt1[2];?></td><td><?=$pt2[2];?></td><td><?=$pt3[2];?></td><td><?=$pt4[2];?></td><td><?=$pt5[2];?></td><td><?=$pt6[2];?></td><td><?=$pt7[2];?></td><td><?=$pt8[2];?></td><td><?=$pt9[2];?></td><td><?=$pt10[2];?></td><td><?=$pt11[2];?></td><td><?=$pt12[2];?></td><td><?=$pt13[2];?></td><td><?=$pt14[2];?></td><td><?=$pt15[2];?></td><td><?=$pt16[2];?></td>
                </tr>				
                <tr align="center">
                    <td>270&deg;</td><td><?=$pt1[3];?></td><td><?=$pt2[3];?></td><td><?=$pt3[3];?></td><td><?=$pt4[3];?></td><td><?=$pt5[3];?></td><td><?=$pt6[3];?></td><td><?=$pt7[3];?></td><td><?=$pt8[3];?></td><td><?=$pt9[3];?></td><td><?=$pt10[3];?></td><td><?=$pt11[3];?></td><td><?=$pt12[3];?></td><td><?=$pt13[3];?></td><td><?=$pt14[3];?></td><td><?=$pt15[3];?></td><td><?=$pt16[3];?></td>
                </tr>
            </tbody>
        </table>
        <table style="width: 100%;padding: 0px;font-size: 9px;" cellpadding="0" cellspacing="0" border="1">
            <thead  bgcolor="#888888" border="1">
                <tr align="center">
                        <th>Angle</th><th>&nbsp;1</th><th>&nbsp;2</th><th>&nbsp;3</th><th>&nbsp;4</th><th>&nbsp;5</th><th>&nbsp;6</th><th>&nbsp;7</th><th>&nbsp;8</th><th>&nbsp;9</th><th>10</th><th>11</th><th>12</th><th>13</th><th>14</th><th>15</th><th>16</th>
                </tr>
            </thead>
            <tbody>
                <tr align="center">
                    <td>0&deg;</td><td>&nbsp;1</td><td>&nbsp;2</td><td>&nbsp;3</td><td>&nbsp;4</td><td>&nbsp;5</td><td>&nbsp;6</td><td>&nbsp;7</td><td>&nbsp;8</td><td>&nbsp;9</td><td>10</td><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td>
                </tr>				
                <tr align="center">
                    <td>90&deg;</td><td>&nbsp;1</td><td>&nbsp;2</td><td>&nbsp;3</td><td>&nbsp;4</td><td>&nbsp;5</td><td>&nbsp;6</td><td>&nbsp;7</td><td>&nbsp;8</td><td>&nbsp;9</td><td>10</td><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td>
                </tr>				
                <tr align="center">
                    <td>180&deg;</td><td>&nbsp;1</td><td>&nbsp;2</td><td>&nbsp;3</td><td>&nbsp;4</td><td>&nbsp;5</td><td>&nbsp;6</td><td>&nbsp;7</td><td>&nbsp;8</td><td>&nbsp;9</td><td>10</td><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td>
                </tr>				
                <tr align="center">
                    <td>270&deg;</td><td>&nbsp;1</td><td>&nbsp;2</td><td>&nbsp;3</td><td>&nbsp;4</td><td>&nbsp;5</td><td>&nbsp;6</td><td>&nbsp;7</td><td>&nbsp;8</td><td>&nbsp;9</td><td>10</td><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td>
                </tr>
            </tbody>
        </table>
        <table style="width: 100%;padding: 0px;font-size: 9px;" cellpadding="0" cellspacing="0" border="1">
            <thead  bgcolor="#888888" border="1">
            <tr align="center">
                <th>Angle</th><th>&nbsp;1</th><th>&nbsp;2</th><th>&nbsp;3</th><th>&nbsp;4</th><th>&nbsp;5</th><th>&nbsp;6</th><th>&nbsp;7</th><th>&nbsp;8</th><th>&nbsp;9</th><th>10</th><th>11</th><th>12</th><th>13</th><th>14</th><th>15</th><th>16</th>
            </tr>
            </thead>
            <tbody>
                <tr align="center">
                    <td>0&deg;</td><td>&nbsp;1</td><td>&nbsp;2</td><td>&nbsp;3</td><td>&nbsp;4</td><td>&nbsp;5</td><td>&nbsp;6</td><td>&nbsp;7</td><td>&nbsp;8</td><td>&nbsp;9</td><td>10</td><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td>
                </tr>				
                <tr align="center">
                    <td>90&deg;</td><td>&nbsp;1</td><td>&nbsp;2</td><td>&nbsp;3</td><td>&nbsp;4</td><td>&nbsp;5</td><td>&nbsp;6</td><td>&nbsp;7</td><td>&nbsp;8</td><td>&nbsp;9</td><td>10</td><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td>
                </tr>				
                <tr align="center">
                    <td>180&deg;</td><td>&nbsp;1</td><td>&nbsp;2</td><td>&nbsp;3</td><td>&nbsp;4</td><td>&nbsp;5</td><td>&nbsp;6</td><td>&nbsp;7</td><td>&nbsp;8</td><td>&nbsp;9</td><td>10</td><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td>
                </tr>				
                <tr align="center">
                    <td>270&deg;</td><td>&nbsp;1</td><td>&nbsp;2</td><td>&nbsp;3</td><td>&nbsp;4</td><td>&nbsp;5</td><td>&nbsp;6</td><td>&nbsp;7</td><td>&nbsp;8</td><td>&nbsp;9</td><td>10</td><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td>
                </tr>
            </tbody>
        </table>
        <div style="padding: 10px;text-align: left;">
            Remarks:
            <?=$list->remarks;?>
        </div>
    </div>
    <div style="margin-top:10px;text-align:center">
	<form action="<?php echo base_url();?>print_data/update_thickness/<?php echo $list->idx;?>" method="post">
	<div class="btn-group">
		<?php if($list->status != "publish"){
			echo "<button type='submit' name='status' value='publish' class='dontprint'>Publish</button>
                            <input type='hidden' name='type' value='kiln'>
			<button type='submit' name='status' value='reject' class='dontprint'>Reject</button>";
		 ?>
             </form>
            <?php
            }elseif ($list->status == "publish"){
                    echo "<input type='button' class='dontprint' onclick='window.print()' value='Print Layout'>";
            }
            ?>
		<input type="button" onClick="location.href='<?php echo base_url();?>record/thickness/kiln'" class='dontprint' value='Back'>
	</div>
	</form>	
    </div>
</center>
<style type="text/css">
    @media print {
    body {-webkit-print-color-adjust: exact;}
    .dontprint{ display: none; }
    @page {size: portrait}
}
</style>