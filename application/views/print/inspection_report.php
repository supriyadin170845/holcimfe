<center>
    <div style='width: 200mm;height:285mm;border: 1px solid;'>
        <table style="width: 100%;padding: 0px;font-size: 15px;" cellpadding="0" cellspacing="0" border="1">
            <tr>
                <td rowspan="5" style="width: 200px;text-align: center;">
                    <img src="<?php echo base_url()?>application/views/assets/img/logo_header.png" height="90px" width="180px">
                </td>
            </tr>
            <tr>
                <td rowspan="2" style="text-align: center;font-size: 18px;">CONDITION BASED MONITORING REPORT</td>
                <td style="width: 110px;">&nbsp;Form Version</td>
                <td style="width: 110px;">&nbsp;: 2.0</td>
            <tr>
                <td>&nbsp;Release Date</td>
                <td>&nbsp;: 01/01/2014</td>
            </tr>

            <tr>
                <td rowspan="2" style="text-align: center;font-size: 21px;">INSPECTION REPORT</td>
                <td>&nbsp;Reported By</td>
                <td>&nbsp;: <?=$list->nama;?></td>
            </tr>
            <tr>
                <td>&nbsp;Reporterd Date</td>
                <td>&nbsp;: <?=date("d-m-Y", strtotime($list->datetime));?></td>
            </tr>
            <tr style="font-size: 10px;">
                <td colspan="4" style="text-transform: uppercase;padding-left: 5px;">
                    <b>HAC :</b> <?=$list->hac_code;?><br/>
                    <b>Equip.Desc:</b> <?=$list->description;?>
                </td>
            </tr>
        </table>
        <table style="width: 100%;padding: 0px;font-size: 12px;padding-left: 5px;padding-top: 5px;padding-bottom: 5px;" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100px">Manufacture</td>
                <td width="700px">: <?=$list->manufacture;?></td>
                <td width="100px">Power</td>
                <td width="150px">: <?=$list->power;?></td>
            </tr>
            <tr>
                <td>Type/Design</td>
                <td>: <?=$list->type;?></td>
                <td>Speed Input</td>
                <td>: <?=$list->speed_input;?></td>
            </tr>
            <tr>
                <td>Year Of Build</td>
                <td>: <?=$list->year_build;?></td>
                <td>Speed Output</td>
                <td>: <?=$list->speed_output;?></td>
            </tr>
            <tr>
                <td>Last Checked</td>
                <td>: <?=$list->last_check;?></td>
                <td>Power</td>
                <td>: <?=$list->power;?></td>
            </tr>
            <tr>
                <td>Last Repaired</td>
                <td>: <?=$list->last_repaired;?></td>
                <td>Power</td>
                <td>: <?=$list->power;?></td>
            </tr>
        </table>
        <table style="width: 100%;padding: 0px;font-size: 15px;" cellpadding="0" cellspacing="0" border="1">
            <tr>
                <td colspan="4" style="text-align: center;font-weight: bolder;">
                    As Found
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center;font-weight: bolder;">
                    <?php
                                        foreach($list_image as $images){
                                    ?>
                    <div style="width: 350px;float: left;margin-left: 13px;font-size: 12px;"><img src="<?php echo base_url();?>media/images/<?php echo $images->image;?>" height="190" width="340" style="padding:10px;"/><span style="margin-top: -40px;"><?=$images->title;?></span></div>
                    
                    
                                        <?php } ?>
                </td>
            </tr>
        </table>
        <div style="padding-top: 10px;padding-left: 5px;text-align: left;">
            <u>Remarks:</u>
            <?=$list->remarks;?>
        </div>
    </div>
     <form action="<?php echo base_url();?>print_data/update_inspection/<?php echo $list->record_id;?>" method="post">
         <div class="btn-group" style="padding-top: 10px;">
            <?php if($list->status == "unpublish"){
                    echo "<button type='submit' name='status' value='publish' class='dontprint'>Publish</button>
                    <button type='submit' name='status' value='reject' class='dontprint'>Reject</button>";
            ?>
             </form>
            <?php
            }elseif ($list->status == "publish"){
                    echo "<input type='button' class='dontprint' onclick='window.print()' value='Print Layout'>";
            }
            ?>
             <input type="button" onClick="location.href='<?php echo base_url();?>record/inspection_report/index/unpublish'" class='dontprint' value='Back' style="display:<?=$display;?>;">
             
             <a href="<?=base_url();?>media/pdf/<?=$list->upload_file;?>" style="display: <?=$display;?>;" target="_blank"><input type="button" value="View PDF" class="dontprint"></a>
	</div>
</center>
<style>
@media print {
body {-webkit-print-color-adjust: exact;}
.dontprint{ display: none; }
@page {size: potrait}

}
</style>