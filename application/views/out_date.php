<link href="<?php echo base_url()?>application/views/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
<link href="<?php echo base_url()?>application/views/assets/bootstrap/css/bootstrap.css" rel="stylesheet"/>
<script type="text/javascript" src="<?php echo base_url() ?>application/views/assets/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>application/views/assets/bootstrap/js/bootstrap.min.js"></script>

<style>
 .center {text-align: center; margin-left: auto; margin-right: auto; margin-bottom: auto; margin-top: auto;}    
</style>

<div class="container" style="position: static;z-index: 9999;">
  <div class="row">
      <div class="col-lg-3"></div>
    <div class="col-lg-4">
      <div class="hero-unit center">
          <h1 style="margin-top: 50px;"> PT.Mitra Fokus Edukasindo </h1>
          <br />
          <p>Program Aplikasi ini telah melampaui waktu trial yang diberikan per tanggal <b><?php echo $detail['expired'][0];?></b></p>
          <p>Silahkan Menghubungi kami di No: 08788 xx xx</p>
                <p>Atau Anda bisa mengirim email melalui tombol dibawah ini:</p>                            
          <center><a href="mailto:info@mfe.co.id" class="btn btn-large btn-info"><i class="icon-home icon-white"></i> Contact US</a></center>
          <p>
                </p>
                <p>Your confirmation code will expire in an hour. Please take action
                  as soon as possible to help us protect your account.</p>
                <p>Thank you,</p>
                <p>Imenchu Security Services Team</p>
        </div>  </div>
        <br />
      </div>
</div>
<?php die; ?>