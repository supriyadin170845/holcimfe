<?php $this->load->view("includes/header.php"); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<!-- Jquery Package End -->
<form method="post" action="<?php echo $form_link; ?>" id="form">
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Edit Form Wizard</h2>
					<h4>Running Inspection Form <span class="pull-right">STEP 1</</span></h4>
					<div class="well well-small">
                                            <table class="table">
							<thead>	
								<tr>
									<td>Forn Name</td>
									<td><input type="text" name="form_name"  class="span6" required value="<?php echo $area_detail->form_name; ?>"/></td>
								</tr>
							</thead>	
							<tbody>	
								<tr>
									<td>Frequency</td>
                                                                        <td><select name="frequency" required >
                                                                                <?php
                                                                                $periode = $area_detail->frequency;
                                                                                $sql=mysql_query("select * from master_frequency");
                                                                                while($data=mysql_fetch_array($sql)){
                                                                                    if($data['id']==$periode){
                                                                                        $cek="selected";
                                                                                    }else{
                                                                                        $cek="";
                                                                                    }
                                                                                    echo"<option value='$data[id]'$cek>$data[frequency]</option>";
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                            <input type="hidden" name="id" value="<?php echo $area_detail->id; ?>"/>
                                                                        </td>
								</tr>
								<tr>
									<td>Mechanical Type</td>
									<td><input type="text" name="mechanical_type"  class="span6" required value="<?php echo $area_detail->mechanichal_type; ?>"/></td>
								</tr>
									<td>Periode</td>
                                                                        <td>
                                                                            <select name="periode" required >
                                                                                <?php
                                                                                $periode = $area_detail->periode;
                                                                                $sql=mysql_query("select * from master_periode");
                                                                                while($data=mysql_fetch_array($sql)){
                                                                                    if($data['id']==$periode){
                                                                                        $cek="selected";
                                                                                    }else{
                                                                                        $cek="";
                                                                                    }
                                                                                    echo"<option value='$data[id]'$cek>$data[periode]</option>";
                                                                                }
                                                                                ?>
                                                                            </select>
                                                                        </td>
								</tr>
                                                                <tr>
									<td>Publish</td>
                                                                        <td>
                                                                            <select name="publish" required >
                                                                                <?php
                                                                                if($area_detail->publish == "Y"){
                                                                                    $yx="selected";
                                                                                }else{
                                                                                    $yx="";
                                                                                }
                                                                                if($area_detail->publish == "N"){
                                                                                    $nx="selected";
                                                                                }else{
                                                                                    $nx="";
                                                                                }
                                                                                echo"
                                                                                <option value='Y' $yx>Yes</option>
                                                                                <option value='N' $nx>No</option>
                                                                                ";
                                                                                ?>
                                                                            </select>
                                                                        </td>
								</tr>
							</tbody>
						</table>
						<button type="submit" class="btn"><i class="icon-check icon-black"></i> Save</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
    $('#form').submit(function(){
     alert('Data has been Update !');
    });
</script>    
<?php $this->load->view("includes/footer.php"); ?>