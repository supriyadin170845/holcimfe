<?php $this->load->view("includes/header.php"); ?>
<form method="post" action="form_manager/simpan_step_running">
    <div id="main">
        <div id="content">
            <div class="inner">	
                <div class="row-fluid">
                    <div class="span12">
                        <h2>Create Form Wizard</h2>
                        <h4>Running Inspection Form <span class="pull-right">STEP 1</</span></h4>
                        <div class="well well-small">
                            <table class="table">
                                <thead>	
                                    <tr>
                                        <td width="200px">Plant Name</td>
                                        <td>
                                            <select name="area" class="span6" required id="plant">
                                                <option value="">-Select Plant-</option>
                                            <?php
                                                foreach ($list_plant as $plant){
                                            ?>
                                                <option value="<?=$plant->id;?>"><?=$plant->plant_name;?></option>
                                            <?php } ?>
                                            </select>
                                        </td>
                                    </tr>	
                                    <tr>
                                        <td>Form Name</td>
                                        <td><input type="text" name="form_name"  class="span6" required/></td>
                                    </tr>
                                </thead>	
                                <tbody>	
                                    <tr>
                                        <td>Frequency</td>
                                        <td>
                                            <select name="frequency" required class="span6">
                                                <option value="">- Select Frequency -</option>
                                                <?php  foreach ($frequency as $data){
                                                        echo "<option value='$data->id' style='text-transform: capitalize;'>$data->frequency</option>";
                                                        }
                                                 ?>";
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Type</td>
                                        <td><input type="text" name="mechanical_type"  class="span6" required/></td>
                                    </tr>
                                    <tr>
                                        <td>Periode</td>
                                        <td>
                                            <select name="periode" required class="span6">
                                                <option value="">- Select Periode -</option>
                                                <?php  foreach ($periode as $data){
                                                        echo "<option value='$data->id' style='text-transform: capitalize;'>$data->periode</option>";
                                                        }
                                                 ?>";
                                            </select>
                                        </td>
                                   </tr>
                                </tbody>
                            </table>
                            <button type="submit" class="btn"><i class="icon-check icon-black"></i> Continue</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
                                            