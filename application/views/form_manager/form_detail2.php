<?php $this->load->view('includes/header.php') ?>

    <div id="content">
	<div class="inner" style="padding-top:2%">
			<div class="btn-group">
				<a href="<?php echo base_url();?>engine/form_manager/edit_detailrunning2/<?php echo $this->uri->segment(4); ?>" class="btn btn-success">Add +</a>
             	                <a href="<?=base_url()?>engine/form_manager/form_detailrunning1" class="btn btn-warning">Back</a>
			</div>
			<div class="well">
			<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
					<tr>
                                            <th style="width: 40px;">NO</th>
						<th>HAC</th>
                        <th>Equipment</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				<?php $id=1; foreach($list as $row) :?>
					<tr>
						<td><?php echo $id++ ?></td>
						<td><?php echo $row->hac_code; ?></td>
                        <td><?php echo $row->equipment_name; ?></td>
						<td style="width: 200px;">
                            <a href="<?php echo base_url();?>engine/form_manager/update_form_detailrunning2/<?php echo $row->form_id; ?>/<?php echo $row->hac;?>" class="btn btn-warning">Update</a>
                            <a href="<?php echo base_url();?>engine/form_manager/form_detailrunning2a/<?php echo $row->form_id; ?>/<?php echo $row->hac; ?>" class="btn btn-primary">Sub</a>
                            <a href="<?php echo base_url();?>engine/form_manager/delete_form_detailrunning2/<?php echo $row->form_id; ?>/<?php echo $row->hac; ?>" class="btn btn-danger" onclick="return confirm('Yakin Hapus HAC ini? Component Dari HAC ini juga akan Di Hapus ! ');">Delete</a>
                        </td>
						
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			</div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>