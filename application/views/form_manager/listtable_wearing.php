<?php $this->load->view('includes/header.php') ?>
<style>
  .pagination{
    text-align: right;
    padding: 20px 0 5px 0;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 10px;
}

.pagination a{
    margin: 0 5px 0 0;
    padding: 3px 6px;
    background: #B3B4BD;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.number{
    border: 1px solid #ddd;
}

.pagination a.current{
    background: #4D6F94;
    border-color: yellow;
    color: #fff !important;
}

.pagination a.curren.hover{
    text-decoration: underline;
}
</style>
    <div id="content">
	<div class="inner" style="padding-top:2%">
		<h2>Wear Measuring</h2>
			<div class="btn-group">
				<a href="<?php echo base_url();?>engine/form_manager/wearing_form_step1" class="btn btn-success">Add</a>
             	
			</div>
			<div class="well">
			<table class="table table-striped">
				<thead style="background-color:#aaaaaa">
 					<tr>
						<th>NO</th>
						<th>HAC</th>
						<th>Area</th>
						<th>Material</th>
						<th style="text-align: center;">Total Publish</th>
                                                <th style="width: 100;text-align: center;">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php 
                                if(count($data)==""){
                                    echo"<tr><td colspan='7' style='text-align:center;'>Data Not Found</td></tr>";
                                }
                                $offset = $this->uri->segment(4);
                                $id= 1; foreach($data as $row) :?>
					<tr>
						<td><?php echo $offset=$offset+1; ?></td>
						<td><?php echo $row->hac_code; ?></td>
						<td><?php echo $row->area_name; ?></td>
						<td><?php echo $row->material; ?></td>
                                                <td style="text-align: center;"><?php echo $row->publish_order; ?></td>
                                                <td style="text-align: center;">
                                                    <button onclick="publish('publish_form_wear/<?php echo $row->id; ?>');" class="btn btn-warning">Publish</button>
                                                    <a href="show_edit_wearingstep1/<?php echo $row->id; ?>" class="btn btn-warning">Edit</a>
                                                    <button onclick="deletex('delete_wear/<?php echo $row->id; ?>');" class="btn btn-warning">Delete</button>
                                                    <a href="print_form_wearing/<?php echo $row->id; ?>" class="btn btn-warning">Print</a>
                                                </td>
						
					</tr>
					<?php endforeach; ?>
                                        <tr>
                                            <td></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/form_manager/wearing_listtable" id="fhac_code"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('hac_code');}else{return false;};" name="val" /><input type="hidden" name="field" value="b.hac_code"></form></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/form_manager/wearing_listtable" id="farea_name"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('area_name');}else{return false;};" name="val" /><input type="hidden" name="field" value="c.area_name"></form></td>
                                            <td><form method="post" action="<?php echo base_url(); ?>engine/form_manager/wearing_listtable" id="fmaterial"><input type="text" style="width: 100px;" onkeyup="javascript:if(event.keyCode == 13){coba('material');}else{return false;};" name="val" /><input type="hidden" name="field" value="a.material"></form></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
				</tbody>
			</table>
			</div>
            <div class="pagination"><?=$halaman;?></div>
		</div>
    </div>
    <?php $this->load->view('includes/footer.php') ?>
<script type="text/javascript">
$(document).ready(function () {
   
});

function publish(id){
   var tanya = confirm("Are you sure?");
        if(tanya){
            var url = "<?php echo base_url(); ?>engine/form_manager/"+id;
            window.location.replace(url);
        }else{
        }
}

function deletex(id){
   var tanya = confirm("Are you sure?");
        if(tanya){
            var url = "<?php echo base_url(); ?>engine/form_manager/"+id;
            window.location.replace(url);
        }else{
        }
}

</script>
<script type="text/javascript">
    $(document).ready(function (){
        $("#coba").keydown(function (e) {
        if (e.keyCode == 13) {
          alert('coba');
        }
    });
    });

    function coba(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
    
    function cobax(tables){
          //alert("valuenya "+id+" fieldnya "+tables);
          document.getElementById("f"+tables).submit();
    }
</script>