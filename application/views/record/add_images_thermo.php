<?php 
$this->load->view('includes/header.php');
?>

<script type="text/javascript">
      var i = 0;       

      function tambah(){
        i++;
		
        var addImages = "<input type='hidden' name='id[]' /><input type='file' name='image[]' />";
        $("#thermoImages tbody").append("<tr class='"+i+"'><td>"+addImages+"</td></tr>")
      };

      function kurang() {
        if(i>0){
          $("#thermoImages tbody tr").remove("."+i);
          i--;
        } else {
          i = 1;
        }
      };
    </script>
	<div id="main">
	<div id="content">
		<div class="inner">
			<div class="row-fluid">
				<div class="span12" style="padding-top:4%">
					<div class="well well-small">
                    
                        <h4>
                            <span class="pull-left">
                                     <div class="btn-group">
                                        <a id="tambah" class="btn btn-info" onclick="tambah();"><i class="icon-plus icon-white"></i>Add</a>
                                        <a id="kurang" class="btn btn-info" onclick="kurang();"><i class="icon-remove icon-white"></i>Delete</a>
                                     </div>
                            </span>
                        </h4>
					<form method="post" action="<?php echo base_url();?>record_insert_images_thermo/<?php echo $id?>">
						<table id="thermoImages" class="table table-bordered">
							<tbody id="listing">	
								<tr class="success">
									<td><strong>IMAGES</strong></td>
								</tr>
								<tr>
									<td> <input type="hidden" name="id[]"/><input type="file" name="image[]"/></td>
                               </tr>
							</tbody>
						</table>
						<button type="submit" class="btn"><i class="icon-check icon-black"></i> Submit</button>
                       </form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
$this->load->view('includes/footer.php');
?>

