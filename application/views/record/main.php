<?php $this->load->view('includes/header.php') ?>
<div id="content">
	<div class="inner" >
			
			<h3>View Record</h3>
			<h4>Select one of inspection type</h4>
			
			<div class="row-fluid">
				
                	<div class="span3">
						
							<a href="<?php echo base_url();?>engine/inspection_manager/list_vrunning_inspection" class=" btn btn-large btn-block">
							  <i class='icon-running icon-inspection-bottom'></i>
                           <br />
							  <h4 class="footertext">Running Inspection</h4>
							 </a>
						
				  </div>
				  <div class="span3">
					
						<a href="<?php echo base_url();?>engine/inspection_manager/list_vstop_inspection" class=" btn btn-large btn-block">
							  <i class='icon-stopin icon-inspection-bottom'></i>
                           <br />
							  <h4 class="footertext">Stop Inspection</h4>
							</a>
					
				  </div>
					<div class="span3">
						
							  <a href="<?php echo base_url();?>record/vibration/index/unpublish" class=" btn btn-large btn-block">
							   <i class='icon-vibration icon-inspection-bottom'></i>
                                 <br />
							  <h4 class="footertext">Vibration Analysis</h4>
							  </a>
						
					</div>
                    <div class="span3">
					
					 <a href="<?php echo base_url();?>record/lubricant/index/unpublish" class=" btn btn-large btn-block">
						   <i class='icon-oil icon-inspection-bottom'></i>
                           <br />
						  <h4 class="footertext">Lubricant Logbook</h4>
						  </a>
					
				  </div>
				
			</div>	  
				 <br/>
			<div class="row-fluid">	 
				  
                  <div class="span3">
    					
    						<a href="<?php echo base_url();?>record/oil_analysis/" class=" btn btn-large btn-block">
    						  <i class='icon-oil icon-inspection-bottom'></i>
                               <br />
    						  <h4 class="footertext">Oil Analysis</h4>
    						  </a>
    					
				  </div>
                  <div class="span3">
					
						<a href="<?php echo base_url();?>record/ultrasonic/index/unpublish" class=" btn btn-large btn-block">
						  <i class='icon-ultrasonic icon-inspection-bottom'></i>
                           <br />
						  <h4 class="footertext">Ultrasonic Test</h4>
						  </a>
					
				  </div>
					<div class="span3">
						
							<a href="<?php echo base_url();?>record/penetrant/index/unpublish" class=" btn btn-large btn-block">
							   <i class='icon-penetration icon-inspection-bottom'></i>
                                <br />
							  <h4 class="footertext">Penetrant Test</h4>
							</a>
					
				  </div>
				  
				<div class="span3">
					
						<a href="<?php echo base_url();?>record/thickness/general/unpublish" class=" btn btn-large btn-block">
						   <i class='icon-thickness icon-inspection-bottom'></i>
                           <br />
						  <h4 class="footertext">Thickness Measurement</h4>
						 </a>
					
				  </div>
			</div>
			<br/>
				<div class="row-fluid">
				  <div class="span3">
					
						<a href="<?php echo base_url();?>record/thermo/index/unpublish" class=" btn btn-large btn-block">
						   <i class='icon-thermo icon-inspection-bottom'></i>
                           <br />
						  <h4 class="footertext">Thermography</h4>
						  </a>
					
				  </div>
                  <div class="span3">
					
							<a href="<?php echo base_url();?>record/mca/index/unpublish" class=" btn btn-large btn-block">
							  <i class='icon-mca icon-inspection-bottom'></i>
                           <br />
							  <h4 class="footertext">MCA</h4>
							 </a>
						
				  </div>
				  <div class="span3">
					
						<a href="<?php echo base_url();?>record/mcsa/index/unpublish" class=" btn btn-large btn-block">
							  <i class='icon-mcsa icon-inspection-bottom'></i>
                           <br />
							  <h4 class="footertext">MCSA</h4>
							</a>
					
				  </div>
                   <div class="span3">
    						
    							<a href="<?php echo base_url();?>record/inspection_report/index/unpublish" class=" btn btn-large btn-block">
    								  <i class='icon-inspection icon-inspection-bottom'></i>
                               <br />
    								  <h4 class="footertext">Inspection Report</h4>
    							</a>
    					   
				    </div>
				 </div>
				 <br/>
				 <div class="row-fluid"> 
				    
                     <div class="span3">
    						
    							<a href="<?php echo base_url();?>engine/inspection_manager/list_vmeasuring_inspection" class=" btn btn-large btn-block">
    								  <i class='icon-inspection icon-inspection-bottom'></i>
                               <br />
    								  <h4 class="footertext">Wear Measurement</h4>
    							</a>
    					   
				    </div>
				
                    <div class="span3">
    						
    							<a href="<?php echo base_url();?>record/other_inspection/index/unpublish" class=" btn btn-large btn-block">
    								  <i class='icon-inspection icon-inspection-bottom'></i>
                               <br />
    								  <h4 class="footertext">Other Report</h4>
    							</a>
    					   
				    </div>
				    
			 
				</div>
				
				 <div class="spacer"></div>
		</div>
  
   </div>

<?php $this->load->view('includes/footer.php') ?>