<?php $this->load->view('includes/header_crud.php') ?>

<div id="main">
    <div id="content">
		<div class="inner">
			<div class="row-fluid">
				<div class="span12">
                                    <a class="btn btn-info" onclick="window.history.back('form_manager/form_detailstop1')" ><i class="icon-arrow-left icon-white"></i> Back</a>
					<?=$output ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="spacer"></div>
			</div>
			<div class="row-fluid">
				<div class="spacer"></div>
			</div>
		</div>
    </div>
</div>

<?php $this->load->view('includes/footer.php') ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>