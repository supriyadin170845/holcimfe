<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style>
    table{
        font-size: 9px;
    }
    table tr td{
        border: 1px solid;
    }
.jtable{
-moz-transform: rotate(-90deg);
-o-transform: rotate(-90deg);
-webkit-transform: rotate(-90deg);
transform: rotate(-90deg);
}
</style>
<script type="text/javascript">
    function printpage() {
        //Get the print button and put it into a variable
        var printButton = document.getElementById("printpagebutton");
        //Set the print button visibility to 'hidden' 
        printButton.style.visibility = 'hidden';
        //Print the page content
        window.print()
        //Set the print button to 'visible' again 
        //[Delete this line if you want it to stay hidden after printing]
        printButton.style.visibility = 'visible';
    }
</script>
</head>

<body>
    <table style="width: 100%;">
  <tr>
      <td width="131" rowspan="4" colspan="2"><img src="<?php echo base_url(); ?>/media/logo.png" height="40px" width="120px"></td>
      <td width="662" rowspan="4" colspan="3"><div align="center">CONDITION BASES MONITORING REPORT<br />
      WEAR MEASUREMENT REPORT<br />
    </div></td>
    <td width="120">Form Version </td>
    <td width="85">: <?=$form->form_number;?></td>
  </tr>
  <tr>
    <td>Release Date</td>
    <td>: <?=$form->publish_date;?></td>
  </tr>
  <tr>
    <td>Inspection Date</td>
    <td>: <?=$form->sys_create_date;?></td>
  </tr>
  <tr>
    <td>Reported By</td>
    <td>: <?php
    $nama = $form->publish_by;
    $nm=mysql_fetch_array(mysql_query("select * from users where id='$nama'"));
    echo $nm['nama'];
    ?></td>
  </tr>
  <tr>
  	<td colspan="8"><div align="center"><?php echo $form->hac_code; ?></div></td>
  </tr>
  <tr>
      <td>-</td>
      <td>meas no: <?php echo $form->meas_no; ?></td>
       <td>Material: <?php echo $form->material; ?></td>
      <td>Date: <?php echo $form->create_date; ?></td>
       <td>Instalation Date: <?php echo $form->instalation_date; ?></td>
       <td colspan="3" width="500px"><div align="center">Depth of Wear (mm) at Measuring Point</div></td>
  </tr>
</table>
    <table style="width: 100%;" style="font-size: 3px;">
        <tr>
            <td colspan="3">Measurement Point</td>
            <td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6</td><td>7</td><td>8</td><td>9</td><td>10</td>
            <td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td><td>17</td><td>18</td><td>19</td><td>20</td>
        </tr>
        <tr>
            <td colspan="3">Distance (mm) from Big Diameter of Roller</td>
            <td style= "width: 20px;text-align: center;">0</td>
            <td style= "width: 20px;text-align: center;">25</td>
            <td style= "width: 20px;text-align: center;">50</td>
            <td style= "width: 20px;text-align: center;">100</td>
            <td style= "width: 20px;text-align: center;">150</td>
            <td style= "width: 20px;text-align: center;">200</td>
            <td style= "width: 20px;text-align: center;">250</td>
            <td style= "width: 20px;text-align: center;">300</td>
            <td style= "width: 20px;text-align: center;">350</td>
            <td style= "width: 20px;text-align: center;">400</td>
            <td style= "width: 20px;text-align: center;">450</td>
            <td style= "width: 20px;text-align: center;">500</td>
            <td style= "width: 20px;text-align: center;">550</td>
            <td style= "width: 20px;text-align: center;">600</td>
            <td style= "width: 20px;text-align: center;">650</td>
            <td style= "width: 20px;text-align: center;">700</td>
            <td style= "width: 20px;text-align: center;">750</td>
            <td style= "width: 20px;text-align: center;">800</td>
            <td style= "width: 20px;text-align: center;">850</td>
            <td style= "width: 20px;text-align: center;">900</td>
        </tr>
        <?php 
            $form_number = $form->form_number;
            $form_id=$form->form_measuring_id;
            $form_id2=$form->id;
            $point = $form->point;
            $sql=  mysql_query("select * from roller where form_id='$form_id'");
            while($data=mysql_fetch_array($sql)){
        ?>
        <tr style='text-align: center;'>
           <?php
           $jmx=  mysql_query("select * from roller_copy where roller_id='".$data['id']."' and expired='1' and form_id <= '$form_id2' order by id ASC");
           $jmxtot=  mysql_num_rows($jmx)+1;
           ?>
            <td rowspan="<?=$jmxtot;?>"><p class="jtable">Roller <?= $data['no_roller'];?></p></td>
            <td valign="bottom"><?= $data['meas_no'];?> - Point Measuring</td>
            <td valign="bottom"><?=$data['date'];?></td>
                <?php
                for($i=1;$i<=20;$i++){
                    echo "<td valign='bottom'>".$data['val'.$i]."</td>";
                }
            ?>  
        </tr>
        <?php
        $sql2=  mysql_query("select * from roller_copy where roller_id='".$data['id']."' and expired='1' and form_id <= '$form_id2' order by id ASC");

        while($data2=  mysql_fetch_array($sql2)){
        ?>
        <tr style='text-align: center;'>

            <td><?= $data2['meas_no'];?> - Point Measuring</td>
            <td><?=$data2['date'];?></td>
                <?php
                for($i=1;$i<=20;$i++){
                    echo "<td>".$data2['val'.$i]."</td>";
                }
                }
                ?>  
        </tr>
        <?php } ?>
        <tr>
            <td colspan="28">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">Measurement Point</td>
            <td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6</td><td>7</td><td>8</td><td>9</td><td>10</td>
            <td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td><td>17</td><td>18</td><td>19</td><td>20</td>
        </tr>
        <tr>
            <td colspan="3">Distance (mm) from Big Diameter of Table</td>
            <td>0</td><td>25</td><td>50</td><td>100</td><td>150</td><td>200</td><td>250</td><td>300</td><td>350</td><td>400</td>
            <td>450</td><td>500</td><td>550</td><td>600</td><td>650</td><td>700</td><td>750</td><td>800</td><td>850</td><td>900</td>
        </tr>
        
        
        <?php 
            $sql=  mysql_query("select * from grinding where form_id='$form_id' group by no_urut");
            while($data4=mysql_fetch_array($sql)){
        ?>
        <tr style='text-align: center;'>
        <?php
        $jmx=  mysql_query("select * from grinding_copy where expired='1' and form_id <= '$form_id2'");
        $jmxtot=  mysql_num_rows($jmx)+4;
        ?>
            <td rowspan="<?=$jmxtot;?>"><p class="jtable">Grinding <?= $data4['no_urut'];?></p></td> 
        </tr>
        <?php 
            $sql6=mysql_query("select * from grinding where form_id='$form_id' and no_urut='".$data4['no_urut']."'");
            while($data6=  mysql_fetch_array($sql6)){
            ?>
            <tr style='text-align: center;'>

                <td valign="bottom"><?= $data6['meas_no'];?> - Point Measuring</td>
                <td valign="bottom"><?=$data6['date'];?></td>
                    <?php
                    for($i=1;$i<=20;$i++){
                        echo "<td valign='bottom'>".$data6['val'.$i]."</td>";
                    }
                ?>  
            </tr>

             <?php
            $sql5=  mysql_query("select * from grinding_copy where  grinding_id='".$data6['id']."' and expired='1' and form_id <= '$form_id2' order by no_urut asc");

            while($data5=  mysql_fetch_array($sql5)){
            ?>
            <tr style='text-align: center;'>

                <td><?= $data5['meas_no'];?> - Point Measuring</td>
                <td><?=$data5['date'];?></td>
                    <?php
                    for($i=1;$i<=20;$i++){
                        echo "<td>".$data5['val'.$i]."</td>";
            }}
                    ?>  
            </tr>

            <?php
            }}
            ?>
    </table>
    <div style="text-align: center;margin-top: 5px;">
    <input id="printpagebutton" type="button" onclick="printpage();" value="Print" >
</div>
</body>
</html>
