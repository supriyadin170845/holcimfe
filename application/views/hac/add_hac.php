<?php $this->load->view("includes/header.php"); ?>
<form method="post" action="<?=base_url();?>engine/crud_hac/add_proses" enctype="multipart/form-data"/>
<div id="main">
	<div id="content">
		<div class="inner">	
			<div class="row-fluid">
				<div class="span12">
					<h2>Add HAC Form</h2>
					<div class="well well-small">
                                            <table class="table">
								<tr>
                                                                    <td width="200px">Plant Name</td>
                                                                    <td>
                                                                        <select name="area" class="span6" required id="plant">
                                                                            <option value="">-Select Plant-</option>
                                                                        <?php
                                                                            foreach ($list_plant as $plant){
                                                                        ?>
                                                                            <option value="<?=$plant->id;?>"><?=$plant->plant_name;?></option>
                                                                        <?php } ?>
                                                                        </select>
                                                                    </td>
								</tr>	
                                                                <tr>
                                                                    <td width="200px">Area Name</td>
                                                                    <td>
                                                                        <select name="area" class="span6" required id="area">
                                                                        </select>
                                                                    </td>
								</tr>
                                                                <tr>
                                                                    <td width="200px">Sub Area Name</td>
                                                                    <td>
                                                                        <select name="subarea" class="span6" required id="subarea">
                                                                        </select>
                                                                    </td>
								</tr>
                                                                <tr id="pl">
                                                                    <td>HAC Code</td>
                                                                    <td>
                                                                        <i style="font-weight: bolder;font-size: 20px;" id="n_area"></i><i style="font-weight: bolder;font-size: 20px;" id="n_plant"></i> <input class="span3" type="text" name="hac_code" maxlength="3"/>
                                                                        <input type="hidden" name="code1" id="n_area_h"/> 
                                                                        <input type="hidden" name="code2" id="n_plant_h"/>
                                                                    </td>
                                                                </tr>
                                                                <tr id="plx">
                                                                    <td>HAC Code</td>
                                                                    <td>
                                                                        <i style="font-weight: bolder;font-size: 20px;" id="n_areax"></i><input style="width: 10px;" type="text" id="plantx" maxlength="1"/><i style="font-size: 40px;">-</i><input class="span3" type="text" name="hac_code_x" maxlength="3"/>
                                                                        <input type="hidden" name="code1_x" id="n_area_hx"/> 
                                                                        <input type="hidden" name="code2_x" id="n_plantx"/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="200px">Equipment</td>
                                                                    <td><input type="number" name="equipment" required class="span6" onkeypress="return validate(event)"/></td>
								</tr>
								<tr>
									<td>Description</td>
                                                                        <td><textarea name="description" style="max-width: 500px;min-width: 500px;max-height: 100px;min-height: 100px;" ></textarea></td>
								</tr>
                                                                <tr>
                                                                    <td width="200px">Func Loc</td>
                                                                    <td><input type="text" name="funcloc" required class="span6"/></td>
								</tr>
                                                                <tr>
									<td>Description Loc</td>
                                                                        <td><textarea name="descriptionloc" style="max-width: 500px;min-width: 500px;max-height: 100px;min-height: 100px;" ></textarea></td>
								</tr>
                                                                <tr>
                                                                    <td width="200px">Indicator</td>
                                                                    <td>
                                                                        <select name="indicator" required>
                                                                            <option value="">-Select Indicator-</option>
                                                                            <option value="A">A</option>
                                                                            <option value="B">B</option>
                                                                            <option value="C">C</option>
                                                                    </td>
								</tr>
                                                                <tr>
                                                                    <td width="200px">Object Type</td>
                                                                    <td><input type="text" name="object_type" required class="span6"/></td>
								</tr>
                                                                <tr>
									<td>Maker Type</td>
                                                                        <td><textarea name="maker_type" style="max-width: 500px;min-width: 500px;max-height: 100px;min-height: 100px;" ></textarea></td>
								</tr>
<!--                                                                <tr>
                                                                    <td width="200px">Planning Plant</td>
                                                                    <td><input type="text" name="planning_plant" required class="span6"/></td>
								</tr>-->
                                                                <tr>
                                                                    <td>Image</td>
                                                                    <td><input type='file' onchange="readURL1(this);" name="image" required />
                                                                        <img id="blah1" src="#" width="150" height="70" />       
                                                                    </td>
                                                                </tr>
						</table>
                                            <button type="submit" class="btn"><i class="icon-check icon-black"></i> Save</button> <a class="btn" onclick="window.history.back();"><i class="icon-backward icon-black"></i> Cancel</a>
					</div>
					<div class="spacer"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<?php $this->load->view("includes/footer.php"); ?>
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"/></script>
<script type="text/javascript" src="<?=base_url();?>tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    theme: "modern",
    width: "750",
    
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   //content_css: "<?=base_url();?>tinymce/css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
}); 
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#plx").hide();   
        $("#pl").hide();
        $("#plantx").keyup(function(){
           $("#n_plantx").val($(this).val()+"-");
        });
        $("#subarea").change(function(){
        var id = $("#area").val();
        var sub=$(this).val();
        $.ajax({
            type:"POST",
            url:"<?=base_url();?>engine/crud_hac/get_data",
            data:"id="+id+"&sub="+sub,
            success: function(dt){
                hasil=dt.split("|");
                console.log(hasil);
                //$("#plant").val(hasil[3].split("|"));
                if(hasil[5].split("|") <=2 ){
                $("#n_area").text("TQ."+hasil[7].split("|"));
                $("#n_plant").html(hasil[5].split("|")+"-");
                $("#n_plant_h").val(hasil[5].split("|")+"-");
                $("#n_area_h").val("TQ."+hasil[7].split("|"));
                $("#pl").show();   
                $("#plx").hide();
                }else{
                    $("#n_areax").text("TQ."+hasil[7].split("|"));
                    $("#n_area_hx").val("TQ."+hasil[7].split("|"));
                    $("#plx").show();   
                    $("#pl").hide();
                }
                
            },
            error: function(dt){
                alert("gagal");
            }
        });
        });
        
        $("#plant").change(function(){
         var id = $("#plant").val();
         $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_area",
             data: "id="+id,
             success: function(data){
                 $("#area").html(data);
             }
         });
      });
      
      $("#area").change(function(){
         var id = $("#area").val();
         $.ajax({
             type:'post',
             url:"<?=base_url();?>engine/crud_hac/get_subarea",
             data: "id="+id,
             success: function(data){
                 $("#subarea").html(data);
             }
         });
      });
    });
    function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah1')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah2')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(70);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function validate(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>