<?php

class Data_form_vibration extends CI_controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index(){
	
		$this->load->view("test_form/vibration");
	}
	
	function add()
	{
		/* -- DO NOT CHANGE -- */
		$user = $this->input->post('user'); // REQUIRE
		$hac = $this->input->post('hac'); // REQUIRE
		$remarks = $this->input->post('remarks'); // REQUIRE
		$recomendation = $this->input->post('recomendation'); // REQUIRE
		$severity_level = $this->input->post('severity_level'); // REQUIRE
		
		$datetime = date('Y-m-d H:i:s'); // REQUIRE
		$date= date('Y-m-d'); // REQUIRE
		/* -- END -- */
		
		
		/* INSPECTION TYPE RECORD */
		/* ubah parameter sesuai dengan record table di Inspection masing2 */
		$description = $this->input->post('description'); 
		$engineer_id = $this->input->post('engineer_id');
		$image_guide = $this->input->post('image_guide');
		$upload_image_1 = $this->input->post('upload_image_1');
		$upload_image_2 = $this->input->post('upload_image_2');
		
		/* PARAM POST/INSERT TO Table record_vibration */
		/* PARAM TO INSERT RECORD to table record */
			$data_post_record = array(
									'hac' => $hac,
									'inspection_type' => 'VIB', // Ubah Sesuai code inspection
									'datetime' => $datetime,
									'remarks' => $remarks,
									'recomendation' => $recomendation,
									'severity_level' => $severity_level,
									'user' => $user
									);
									
			/* PROCESS TO INSERT */						
			
		
		/* ---- end --- */
		
		/* PROCESS INSERT TO Table record_vibration */
		if($this->db->insert('record',$data_post_record))
		{
			$data_post['status'] = 'Success';
			
			$record_id = mysql_insert_id(); // Ambil Primary KEY dari Insert Record Inpection
			
			
			$data_post = array(
						'record_id' => $record_id,
						'date_vibration' => $date,
						'description' => $description,
						'inspector_id' => $user,
						'engineer_id' => $engineer_id,
						'image_guide' => $image_guide,
						'upload_image_1' => $upload_image_1,
						'upload_image_2' => $upload_image_2
						);
			
			$this->db->insert('record_vibration',$data_post);
			
			$inspection_id = mysql_insert_id();
		
			$data_update_inspection = array('inspection_id' => $inspection_id);
			$where = array('id' => $record_id);
			
			$this->db->update('record', $data_update_inspection, $where);
			
		}
		else
		{
			$data_post['status'] = 'Failed';
		}
		
		$data_json = array('record' => $data_post_record, 'record_detail'=> $data_post);
		
		echo json_encode($data_json);
			
					
	}
}