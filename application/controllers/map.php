<?php

class Map extends CI_controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('access');
		$this->load->helper(array('form', 'url'));
	}

	function detail_1()
	{
		
		$this->load->view('map/detail_1');
	}
	
	
	function detail_2()
	{
		
		$this->load->view('map/detail_2');
	}
	
	function detail_3()
	{
		
		$this->load->view('map/detail_3');
	}
	
	function detail_4()
	{
		
		$this->load->view('map/detail_4');
	}
	
	function detail_5()
	{
		
		$this->load->view('map/detail_5');
	}
	
	
}	