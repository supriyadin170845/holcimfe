<?php

class Add_others extends CI_controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('grocery_crud');
                $this->load->model('users_model');
                $this->load->model('form_manager_model');	
	}
	
        function insert_log_activity($type,$primarykey,$description){
            $this->load->model('form_manager_model');
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>$description,
                "type"=>$type,
                "record_id"=>$primarykey
            );
            $this->form_manager_model->log_activity($data);
        }
        
        function remark_engineer($record_id,$status,$remarks,$type){
            $data=array(
                'record_id'=>$record_id,
                'remarks'=>$remarks,
                'inspector_id'=>$this->session->userdata('users_id'),
                'date_remarks'=>date("Y-m-d H:i:s"),
                'publish'=>'1',
                'type_report'=>$type,
                'status'=>$status
            );
            $this->db->insert('engineer_remark',$data);
        }
        
	function index()
	{
                $val=$this->input->post('val');
                $fieldx = $this->input->post('field');
                if($fieldx==""){
                    $field="re.id";
                }else{
                    $field=$fieldx;
                }
		$config['base_url'] = base_url().'record/add_others/index/';
                $config['total_rows'] = $this->db->query("select * from record re, record_other_report reoth, hac where re.inspection_type='OTHERS' and re.hac=hac.id and $field LIKE '%$val%' group by re.id")->num_rows();
                $config['per_page'] = 10;
                $config['num_links'] = 2;
                $config['uri_segment'] = 4;
                $config['first_page'] = 'Awal';
                $config['last_page'] = 'Akhir';
                $config['next_page'] = '&laquo;';
                $config['prev_page'] = '&raquo;';
                $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
                //inisialisasi config
                $this->pagination->initialize($config);
                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                //tamplikan data
		$data['data'] = $this->db->query("select *,re.id as idx,re.severity_level from record re, record_other_report reoth, hac where re.inspection_type='OTHERS' and re.hac=hac.id  and $field LIKE '%$val%' group by re.id limit ".$pg.",".$config['per_page']."")->result();
   	    
		$this->load->view('record/add_others', $data); 
	}
	
	function add()
	{
            $data['list_plant']=$this->users_model->select_all("master_plant")->result();
            $this->load->view('record/form_add_others',$data); 
	}
	
        function add_post()
	{
                /* -- DO NOT CHANGE -- */
		$user = $this->input->post('user'); // REQUIRE
		$hac = $this->input->post('hac'); // REQUIRE
		$remarks = $this->input->post('remarks'); // REQUIRE
		$recomendation = $this->input->post('recomendation'); // REQUIRE
		$severity_level = $this->input->post('severity_level'); // REQUIRE
		$datetime = date('Y-m-d H:i:s'); // REQUIRE
		$date= date('Y-m-d'); // REQUIRE
                $area=$this->input->post('subarea');
                $description=$this->input->post('description');
                
                //get hac id
                $get_idhac = $this->form_manager_model->get_idhac($hac);
                
                //insert into table record
                $data_record=array(
                                    'hac'=>$get_idhac,
                                    'inspection_type'=>'OTHERS',
                                    'inspection_id'=>'',
                                    'datetime'=>$datetime,
                                    'remarks'=>$remarks,
                                    'recomendation'=>$recomendation,
                                    'severity_level'=>$severity_level,
                                    'status'=>'unpublish',
                                    'user'=>$user,
                                    'publish_by'=>''
                                    );
                $this->users_model->insert("record",$data_record);
        
		//upload file
                $config['upload_path']	= "./media/pdf/";
                $config['upload_url']	= base_url().'media/pdf/';
                $config['allowed_types']= '*';
                $config['max_size']     = '2000000';
                $config['max_width']  	= '2000000';
                $config['max_height']  	= '2000000';
                $this->load->library('upload');
                $this->upload->initialize($config);

                if($this->upload->do_upload('upload_file'))
                 {
                $image_data1 = $this->upload->data();    
                 }
                
                //select max id record
                $idmax=$this->users_model->get_max_table("id","record");
                
                //insert into table RECORD_OTHERS
                $data_record_vibration=array(
                                            'record_id'=>$idmax,
                                            'description'=>$description,
                                            'area'=>$area,
                                            'upload_file'=>$image_data1['file_name'],
                                            'date'=>$date,
                                            'hac'=>$get_idhac

                                            );
                $this->users_model->insert("record_other_report",$data_record_vibration);
		
                $this->load->library('upload');

                $files = $_FILES;
                $cpt = count($_FILES['userfile']['name']);
                for($i=0; $i<$cpt; $i++)
                {

                    $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                    $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                    $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                    $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                    $_FILES['userfile']['size']= $files['userfile']['size'][$i];    



                $this->upload->initialize($this->set_upload_options());
                $this->upload->do_upload();
                $data_image=array('record_id'=>$idmax,'image'=>$_FILES['userfile']['name']);
		$this->db->insert('record_other_image',$data_image);
                }
                
                //Insert into engineer Remak
                $this->remark_engineer($idmax,'NEW','Add New Record Other Report','Other Report');
                //insert into activity log
                $this->insert_log_activity("Record Other Report",$idmax,"Create New Record Other Report");
                
		redirect("record/add_others/"); 
			
					
	}
	
	
	function edit($id){
	$data['list_image']=$this->users_model->select_all_where("record_other_image",$id,"record_id")->result();
        $data['list_plant']=$this->users_model->select_all("master_plant")->result();
        $data['list']=$this->db->query("select a.*,b.*,c.hac_code from record a inner join record_other_report b on b.record_id=a.id inner join hac c on a.hac=c.id where a.id='$id'")->row();
	$this->load->view('record/form_edit_others', $data);
	
	}
	
	
	function autocomplete_hac(){
		$keyword = $this->input->post("term");
		$result = $this->db->query('select * from hac where hac_code like "'.$keyword.'%" LIMIT 10')->result_array();
		foreach($result as $row){
			$data[] = array('label'=>$row['hac_code'], 'value'=>$row['hac_code']);
		}
		echo json_encode($data);
	}
	
	
	function autocomplete_hac_detail(){
		$hac = $this->input->post("term");
		
		$main = $this->db->query('select * from hac where hac_code="'.$hac.'"')->row_array();
		$data = array(
			"main" => $main
		);
		
		echo json_encode($data);
	}
	
	function edit_post(){
	$id = $this->input->post("id");
        /* -- DO NOT CHANGE -- */
        $user = $this->input->post('user'); // REQUIRE
        $hac = $this->input->post('hac'); // REQUIRE
        $remarks = $this->input->post('remarks'); // REQUIRE
        $recomendation = $this->input->post('recomendation'); // REQUIRE
        $severity_level = $this->input->post('severity_level'); // REQUIRE
        $datetime = date('Y-m-d H:i:s'); // REQUIRE
        $date= date('Y-m-d'); // REQUIRE
        $area=$this->input->post('subarea');
        $description=$this->input->post('description');
        $upload_file_hidden=$this->input->post('upload_file_hidden');
        $userfile_hidden=$this->input->post('userfile_hidden');

        //get hac id
        $get_idhac = $this->form_manager_model->get_idhac($hac);

        //insert into table record
        $data_record=array(
                            'hac'=>$get_idhac,
                            'inspection_type'=>'OTHERS',
                            'inspection_id'=>'',
                            'datetime'=>$datetime,
                            'remarks'=>$remarks,
                            'recomendation'=>$recomendation,
                            'severity_level'=>$severity_level,
                            'status'=>'unpublish',
                            'user'=>$user,
                            'publish_by'=>''
                            );
       $this->users_model->update("record",$id,"id",$data_record);

        //upload file
        $config['upload_path']	= "./media/pdf/";
        $config['upload_url']	= base_url().'media/pdf/';
        $config['allowed_types']= '*';
        $config['max_size']     = '2000000';
        $config['max_width']  	= '2000000';
        $config['max_height']  	= '2000000';
        $this->load->library('upload');
        $this->upload->initialize($config);

        if($this->upload->do_upload('upload_file'))
         {
            $image_data1 = $this->upload->data();    
            $img=$image_data1['file_name'];
         }else{
             $img=$upload_file_hidden;
         }

        //insert into table RECORD_OTHERS
        $data_record_vibration=array(
                                    'record_id'=>$id,
                                    'description'=>$description,
                                    'area'=>$area,
                                    'upload_file'=>$img,
                                    'date'=>$date,
                                    'hac'=>$get_idhac

                                    );
        $this->users_model->update("record_other_report",$id,"record_id",$data_record_vibration);
        
        $this->users_model->delete('record_other_image',$id,'record_id');
        if(!$userfile_hidden){

        }else{
            for($i=0;$i<count($userfile_hidden);$i++){
                $data_image=array('record_id'=>$id,'image'=>$userfile_hidden[$i]);
                $this->db->insert('record_other_image',$data_image);
            }
        }
        $this->load->library('upload');

        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);
        for($i=0; $i<$cpt; $i++)
        {

            $_FILES['userfile']['name']= $files['userfile']['name'][$i];
            $_FILES['userfile']['type']= $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error']= $files['userfile']['error'][$i];
            $_FILES['userfile']['size']= $files['userfile']['size'][$i];    



        $this->upload->initialize($this->set_upload_options());
        if($this->upload->do_upload()){
        $data_image=array('record_id'=>$id,'image'=>$_FILES['userfile']['name']);
        $this->db->insert('record_other_image',$data_image);
        }
        }
        
        //Insert into engineer Remak
            $this->remark_engineer($id,'UPDATE','Update Record Vibration Other Report','Other Report');
            
            //insert into activity log
            $this->insert_log_activity("Record Other Report",$id,"Update Record Other Report");

        redirect("record/add_others/"); 
	
	}
	
	function delete($id){
            $this->users_model->delete("record",$id,"id");
            $this->users_model->delete("record_other_report",$id,"record_id");
            $this->users_model->delete("record_other_image",$id,"record_id");
            
            //insert into activity log
            $this->insert_log_activity("Record Other Report",$id,"Delete Record Other Report");
            redirect("record/add_others/"); 
        }
	
        function set_upload_options()
        {   
        //  upload an image options
            $config = array();
            $config['upload_path']	= "./media/images/";
            $config['upload_url']	= base_url().'media/images/';
            $config['allowed_types']= '*';  
            $config['max_size']	= '100000';
            $config['max_width'] = '1024000';
            $config['max_height'] = '768000';
            $config['overwrite']     = FALSE;
            return $config;
        }
	
	
}	

	