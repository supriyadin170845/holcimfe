<?php

class Stop_inspection extends CI_controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('grocery_crud');	
	}
	

	function index($status = 'unpublish', $type_form = null)
	{
		
		$hac = $this->input->post('hac');
		
		$crud = new grocery_CRUD();
		$crud->set_theme('datatables');
        $crud->set_table('record');
        $crud->set_subject('Stop_inspection');
		$crud->columns('hac', 'inspection_type', 'datetime', 'status','user');
		$crud->add_action('View', '', 'print_data/stop_inspection','ui-icon-plus');
		
		// $crud->where('inspection_type','STOP');
		$crud->where('status',$status);
		
		if($type_form!= null)
		{
			$crud->where('inspection_type',$type_form);
		}
		else
		{
			$crud->where('inspection_type','STOP_1M');
			$crud->or_where('inspection_type','STOP_3M');
			$crud->or_where('inspection_type','STOP_6M');
			$crud->or_where('inspection_type','STOP_1Y');
		}
		
		if($hac != null)
		{
			$data_hac = $this->main_model->get_detail('hac',array('hac_code' =>$hac));
			$crud->where('hac',$data_hac['id']);
		}
		
		$crud->set_relation('hac','hac','hac_code');
		// $crud->set_relation('user','users','nip');
		
		$crud->unset_export();
		$crud->unset_read();
		$crud->unset_print();
		$crud->unset_add();
		$crud->unset_edit();
		$crud->unset_delete();
        
		if($this->session->userdata('users_level') == 'Inspector')
		{
			$crud->where('inspector_id',$this->session->userdata('users_id'));
		}
		
		$crud->callback_column('user',array($this,'call_back_collom_user'));
		
        $output = $crud->render();
 
        $this->output($output);
		
		
		
	}
	
	function call_back_collom_user($value, $row){
	
		$data_user = $this->main_model->get_detail('users',array('id'=> $row->user));
		
		return $data_user['nip']." - ".$data_user['nama'];
	
	
	}
	
	
	
	
	function output($output = null)
    {
        $this->load->view('record/page_stop_inspection.php',$output);    
    }
}	

	