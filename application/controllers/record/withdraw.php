<?php

class Withdraw extends CI_controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('users_model');
		$this->load->library('grocery_crud');	
	}
        
        function insert_log_activity($type,$primarykey,$description){
            $this->load->model('form_manager_model');
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>$description,
                "type"=>$type,
                "record_id"=>$primarykey
            );
            $this->form_manager_model->log_activity($data);
        }
	function remark_engineer($record_id,$status,$remarks,$type){
            $data=array(
                'record_id'=>$record_id,
                'remarks'=>$remarks,
                'inspector_id'=>$this->session->userdata('users_id'),
                'date_remarks'=>date("Y-m-d H:i:s"),
                'publish'=>'1',
                'type_report'=>$type,
                'status'=>$status
            );
            $this->db->insert('engineer_remark',$data);
        }
        
        public function index(){
        //pengaturan pagination
        $val=$this->input->post('val');
        $fieldx = $this->input->post('field');
        if($fieldx==""){
            $field="a.id";
        }else{
            $field=$fieldx;
        }
        $config['base_url'] = base_url().'record/withdraw/index/';
        $config['total_rows'] = $this->db->query("select a.*,b.nip,nama,c.area_name from record_withdraw_oil a left join users b on a.user=b.id left join area c on a.subarea_id=c.id where $field like '%$val%' order by a.id desc")->num_rows();
        $config['per_page'] = 20;
        $config['num_links'] = 2;
        $config['uri_segment'] = 4;
        $config['first_page'] = 'Awal';
        $config['last_page'] = 'Akhir';
        $config['next_page'] = '&laquo;';
        $config['prev_page'] = '&raquo;';
        $pg = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0 ;
        //inisialisasi config
        $this->pagination->initialize($config);
        //buat pagination
        $data['halaman'] = $this->pagination->create_links();
        //tamplikan data
        $data['list']=$this->db->query("select a.*,b.nip,nama,c.area_name from record_withdraw_oil a left join users b on a.user=b.id left join area c on a.subarea_id=c.id where $field like '%$val%' order by a.id desc limit $pg,$config[per_page]")->result();
        $this->load->view('record/withdraw', $data); 
    }
    
    function add(){
        $data['list_plant']=$this->users_model->select_all('master_plant')->result();
        $data['user']=$this->db->query("select * from users where id='".$this->session->userdata('users_id')."'")->row();
        $this->load->view('record/add_withdraw',$data);
    }
    
    function add_proses(){
        $id_user=$this->input->post('id_user');
        $wo=$this->input->post('wo');
        $date=$this->input->post('date');
        $subarea=$this->input->post('subarea');
        
        $data=array(
            'work_order'=>$wo,
            'user'=>$id_user,
            'date'=>$date,
            'subarea_id'=>$subarea,
            'sys_create_date'=>date("Y-m-d h:i:s")
        );
        $this->db->insert('record_withdraw_oil',$data);
        $id = mysql_insert_id();
        $this->insert_log_activity("Report Withdraw Oli",$id,"Create New Record Withdraw Oli '$id'");
        //Insert into engineer Remak
        $this->remark_engineer($id,'NEW','Add New Record Withdraw Oli Lubricant Log Book');
        echo "<script>alert('Data Saved');
        window.location.href='".base_url()."record/withdraw'</script>";
        redirect('record/withdraw');
    }
    
    function edit($id){
        $data['list_plant']=$this->users_model->select_all("master_plant")->result();
        $data['list']=$this->db->query("select a.*,b.nama from record_withdraw_oil a left join users b on a.user=b.id where a.id='$id'")->row();
        $this->load->view('record/edit_withdraw',$data);
    }
    
    function edit_proses(){
        $id=$this->input->post('id');
        $id_user=$this->input->post('id_user');
        $wo=$this->input->post('wo');
        $date=$this->input->post('date');
        $subarea=$this->input->post('subarea');
        
        $data=array(
            'work_order'=>$wo,
            'user'=>$id_user,
            'date'=>$date,
            'subarea_id'=>$subarea
        );
        $this->db->where('id',$id);
        $this->db->update('record_withdraw_oil',$data);
        $this->insert_log_activity("Report Withdraw Oli",$id,"Update Record Withdraw Oli '$wo'");
        //Insert into engineer Remak
        $this->remark_engineer($id,'UPDATE','Update Record Withdraw Oli','Lubricant Log Book');
        redirect('record/withdraw');
    }
    
    function delete($id){
        $data=$this->db->query("select * from record_withdraw_oil where id='$id'")->row();
        $this->insert_log_activity("Report Withdraw Oli",$id,"Delete Record Withdraw Oli '$data->work_order'");
        $this->db->query("delete from record_withdraw_oil_list where record_withdraw_id='$id'");
        $this->db->query("delete from record_withdraw_oil where id='$id'");
        redirect('record/withdraw');
    }
}	