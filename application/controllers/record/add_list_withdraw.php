<?php

class Add_list_withdraw extends CI_controller {

	function __construct()
	{
		parent::__construct();	
                $this->load->model('users_model');
                $this->load->model('form_manager_model');
		$this->load->library('grocery_crud');	
	}
	
        function insert_log_activity($type,$primarykey,$description){
            $this->load->model('form_manager_model');
            $data = array(
                "user_id"=>$this->session->userdata('users_id'),
                "date_activity"=>date("Y-m-d h:i:s"),
                "description"=>$description,
                "type"=>$type,
                "record_id"=>$primarykey
            );
            $this->form_manager_model->log_activity($data);
        }
        
	function index($id)
	{
                $val=$this->input->post('val');
                $fieldx = $this->input->post('field');
                if($fieldx==""){
                    $field="id";
                }else{
                    $field=$fieldx;
                }
                $config['base_url'] = base_url().'record/add_list_withdraw/index/'.$id;
                $config['total_rows'] = $this->db->query("select * from record_withdraw_oil_list where record_withdraw_id='$id' and $field LIKE '%$val%' order by id desc")->num_rows();
                $config['per_page'] = 20;
                $config['num_links'] = 2;
                $config['uri_segment'] = 5;
                $config['first_page'] = 'Awal';
                $config['last_page'] = 'Akhir';
                $config['next_page'] = '&laquo;';
                $config['prev_page'] = '&raquo;';
                $pg = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0 ;
                //inisialisasi config
                $this->pagination->initialize($config);
                //buat pagination
                $data['halaman'] = $this->pagination->create_links();
                //tamplikan data
                $data['idx']=$id;
		$data['data'] = $this->db->query("select * from record_withdraw_oil_list where record_withdraw_id='$id' and $field LIKE '%$val%' order by id desc limit ".$pg.",".$config['per_page']."")->result();
   	    
		$this->load->view('record/add_list_withdraw', $data); 
	}
	
	function add($id)
	{
            $data['id']=$id;
            $this->load->view('record/form_add_list_withdraw',$data); 
	}
        
    function add_proses(){
        $record_withdraw_id=$this->input->post('id');
        $batch_no=$this->input->post('batch_no');
        $lubricant=$this->input->post('lubricant');
        $qty=$this->input->post('qty');
        $unit=$this->input->post('unit');
        
        for($i=0;$i<count($batch_no);$i++){
           $data=array('record_withdraw_id'=>$record_withdraw_id,
                       'batch_no'=>$batch_no[$i],
                       'lubricant'=>$lubricant[$i],
                       'qty'=>$qty[$i],
                       'unit'=>$unit[$i],
                       'sys_create_date'=>date("Y-m-d h:i:s")
                    );
           $this->db->insert("record_withdraw_oil_list",$data);
        }
        redirect('record/add_list_withdraw/index/'.$record_withdraw_id);
    }
	
    function edit($idc,$id){
        $data['idx']=$idc;
        $data['list']=$this->db->query("select * from record_withdraw_oil_list where id='$id'")->row();
        $this->load->view('record/form_edit_list_withdraw', $data);
	
    }
    
    function edit_proses(){
        $idx=$this->input->post('idx');
        $id=$this->input->post('id');
	$batch_no=$this->input->post('batch_no');
        $lubricant=$this->input->post('lubricant');
        $qty=$this->input->post('qty');
        $unit=$this->input->post('unit');
        
        $data=array(
            'batch_no'=>$batch_no,
            'lubricant'=>$lubricant,
            'qty'=>$qty,
            'unit'=>$unit
        );
        $this->db->where('id',$id);
        $this->db->update('record_withdraw_oil_list',$data);
        redirect('record/add_list_withdraw/index/'.$idx);
    }
	
        function delete($idx,$id){
            $this->db->where('id',$id);
            $this->db->delete('record_withdraw_oil_list');
            redirect('record/add_list_withdraw/index/'.$idx);
        }
	
	
	
	
}	

	