<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inbox extends CI_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->library('access');
		$this->load->helper(array('form', 'url', 'text'));
	}
	
    function index(){
			

            $this->access->check_access();
            $this->load->view('includes/header');
			$this->load->view('mail/list_message');
			$this->load->view('includes/footer');                       
	}
    
     function send_admin(){
            $this->access->check_access();
            $name = $this->input->post("name"); 
            $department = $this->input->post("department");  
            $message = $this->input->post("message");  
            $email = $this->input->post("email");  
            
            $data = array(
                "name" => $name,
				"department" => $department,
				"message" => $message,
                                "email"=>$email
            );
            $this->db->insert('admin_inbox', $data);
            
            redirect("main/view_contact/");                     
	}
    

         
	function readmsg($id){
           $this->access->check_access();
           $data['master_data']=$this->db->query("select * from admin_inbox where id='$id'")->row();
           $this->db->query("update admin_inbox set publish='1' where id='$id'");
           $this->load->view('includes/header');
           $this->load->view('mail/read_message', $data);
           $this->load->view('includes/footer');
	}
        
        function read_outbox($id){
           $this->access->check_access();
           $data['master_data']=$this->db->query("select a.*,b.nama,level from pm a left join users b on a.user2=b.id where a.id='$id'")->row();
           $this->load->view('includes/header');
           $this->load->view('mail/read_outbox', $data);
           $this->load->view('includes/footer');
	}
	
    
    	function read_admin($id){
            $this->access->check_access();
            $data['idx']=$id;
            $this->db->query("update pm set user2read='yes' where id='$id'");
            $id2=$this->db->query("select id2 from pm where id='$id' limit 1")->row('id2');
	    $data['read'] = $this->db->query("select a.*,b.nama from pm a left join users b on a.user1=b.id where a.id2='$id2'  order by id asc")->result();
            $data['id']=$this->db->query("select id2 from pm where id2='$id2' limit 1")->row('id2');
            $data['user'] = $this->db->query("select user1 from pm where id2='$id2' and id='$id' limit 1")->row('user1');
            $this->load->view('includes/header');
            $this->load->view('mail/read_admin', $data);
            $this->load->view('includes/footer');
	}
        
        function read_inspector($id){
            $this->access->check_access();
            $data['idx']=$id;
            $this->db->query("update pm set user2read='yes' where id='$id'");
            $id2=$this->db->query("select id2 from pm where id='$id' limit 1")->row('id2');
	    $data['read'] = $this->db->query("select a.*,b.nama from pm a left join users b on a.user1=b.id where a.id2='$id2'  order by id asc")->result();
            $data['id']=$this->db->query("select id2 from pm where id2='$id2' limit 1")->row('id2');
            $data['user'] = $this->db->query("select user1 from pm where id2='$id2' and id='$id' limit 1")->row('user1');
            $this->load->view('includes/header');
            $this->load->view('mail/read_inspector', $data);
            $this->load->view('includes/footer');
	}
    
    function read_engineer($id){
            $this->access->check_access();
            $data['idx']=$id;
            $this->db->query("update pm set user2read='yes' where id='$id'");
            $id2=$this->db->query("select id2 from pm where id='$id' limit 1")->row('id2');
	    $data['read'] = $this->db->query("select a.*,b.nama from pm a left join users b on a.user1=b.id where a.id2='$id2' order by id asc")->result();
            $data['id']=$this->db->query("select id2 from pm where id2='$id2' limit 1")->row('id2');
            $data['user'] = $this->db->query("select user1 from pm where id2='$id2' and id='$id' limit 1")->row('user1');
            $this->load->view('includes/header');
            $this->load->view('mail/read_engineer', $data);
            $this->load->view('includes/footer');
	}
    
    function send_read(){
            $this->access->check_access();
            $id2 = $this->input->post("id");
            $user1 = $this->session->userdata['users_id'];  
            $user2 = $this->input->post("user2");
            $user1read = $this->input->post("user1read");
            $message = $this->input->post("message");  
            $title_message = $this->input->post("title_message"); 
            
            $data = array(
				"id2" => $id2,
                "title_message" => $title_message,
                "user1"=> $user1,
                "user2"=> $user2,
                 "user1read"=> $user1read,
				"message" => $message
            );
            $this->db->insert('pm', $data);
            
            //redirect("inbox/readmsg/".$id); 
            redirect('inbox/index');                     
	}
    
	
	function outbox(){
            $this->access->check_access();
            $this->load->view('includes/header');
            $this->load->view('mail/outbox_message');
            $this->load->view('includes/footer');
	}
        
	function newmsger($msg=NULL){
                   
           $data['msg']=$msg;
           $this->load->helper(array('form','url'));
           $data['obj']=  $this->session;
           $this->load->view('includes/header');
		   $this->load->view('mail/new_message',$data);
           $this->load->view('includes/footer');
                        
	}
	
	
	
	function cari_orang(){
		$this->load->model('test_msg');
	 
		$keyword = $this->input->post("keyword", true);
		$result = $this->test_msg->get_orang($keyword);
		foreach($result->result_array() as $row){
		
			$data[] = array('label'=>$row['nama'], 'value'=>$row['nama']);
		}
		echo json_encode($data);
	}
	
	function newmsgs(){
     
	   $user1 = $this->session->userdata('users_id');
           $user2= $this->input->post('user');
           $message= $this->input->post('message');
           $data = array(
                "user1" => $user1,
                "user2" => $user2,
                "message" => $message
            );
            $this->db->insert('pm', $data);
            
            $id = mysql_insert_id();
            $query = array(
                "id2" => $id
            );
            
            $this->db->where('id', $id);
            $this->db->update('pm', $query);
            
            redirect("inbox/index");           
		}   
        
        function engineer_remark($id){
            $data['master_data']=$this->db->query("select a.*,c.hac_code,d.nama,d.level from engineer_remark a inner join record b on a.record_id=b.id inner join hac c on b.hac=c.id inner join users d on a.inspector_id=d.id where a.id='$id'")->row();
            $this->db->query("update engineer_remark set read_status='1' where id='$id'");
            $this->load->view('includes/header');
            $this->load->view('mail/read_sis_engineer',$data);
            $this->load->view('includes/footer');
        }
        
        function inspector_remark($id){
            $data['master_data']=$this->db->query("select a.*,c.hac_code,d.nama,d.level from inspector_remark a inner join record b on a.record_id=b.id inner join hac c on b.hac=c.id inner join users d on a.engineer_id=d.id where a.id='$id'")->row();
            $this->db->query("update inspector_remark set read_status='1' where id='$id'");
            $this->load->view('includes/header');
            $this->load->view('mail/read_sis_inspector',$data);
            $this->load->view('includes/footer');
        }
        
        function reply(){
            $idx=$this->input->post('idx');
            $id2=$this->input->post('id2');
            $user2=$this->input->post('user2');
            $message=$this->input->post('message');
            $data = array(
                'id2'=>$id2,
                'user1'=>$this->session->userdata('users_id'),
                'user2'=>$user2,
                'message'=>$message
            );
            $this->db->insert('pm',$data);
            redirect('inbox/read_engineer/'.$idx);
        }
        
      function get_user(){
        $data="";
        $id=$this->input->post('id');
        $val=$this->db->query("select * from users where level = '$id'")->result();
        $data .= "<option value=''>--pilih--</option>";
        foreach($val as $value){
            $data .="<option value='$value->id'>$value->nama</option>\n";
        }
        echo $data;
     }
     
     function get_edit_user(){
        $data="";
        $id=$this->input->post('id');
        $level=$this->input->post('level');
        $val=$this->db->query("select * from users where level = '$level'")->result();
        $data .= "<option value=''>--pilih--</option>";
        foreach($val as $value){
            if($id==$value->id){
                $cek="selected";
            }else{
                $cek="";
            }
            $data .="<option value='$value->id'$cek>$value->nama</option>\n";
        }
        echo $data;
     }
     
     function get_image(){
        $data="";
        $id=$this->input->post('id');
        $val=$this->db->query("select * from users where id = '$id'")->row();
        $data .= $val->photo;
        echo $data;
     }
     
     function delete_message(){
         $id=$this->input->post('id');
         $this->db->query("update pm set user2hide='1', user2read='yes' where id='$id'");
         return true;
     }
     
    function delete_general(){
         $id=$this->input->post('id');
         $this->db->query("delete from admin_inbox where id='$id'");
         return true;
     }
     
     function message_top(){
        $user=$this->input->post('id');
        $list=$this->db->query("select * from users where id='$user'")->row();
        if($list->level=="Engineer"){
            $sis_msg=$this->db->query("select * from engineer_remark where read_status='0'")->num_rows(); 
            $pm=$this->db->query("select * from pm  where  user2='".$user."' and user2read='no'")->num_rows(); 
            $req3 = intval($sis_msg) + intval($pm);
            echo $req3;
        }
        if($list->level=="Inspector"){
            $sis_msg=$this->db->query("select * from inspector_remark where read_status='0'")->num_rows(); 
            $pm=$this->db->query("select * from pm  where  user2='".$user."' and user2read='no'")->num_rows(); 
            $req3 = intval($sis_msg) + intval($pm);
            echo $req3;
        }
        if($list->level=="Administrator"){
            $admin_inbox=$this->db->query("select * from admin_inbox  where publish='0'")->num_rows();
            $pm=$this->db->query("select * from pm  where  user2='".$user."' and user2read='no'")->num_rows(); 
            $req3 = intval($admin_inbox) + intval($pm);
            echo $req3;
        }
     }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */