<?php

class Access_web extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();	
		$this->load->library('access');
		$this->user = null;
	}
	
	function index()
	{
		$this->load->view('test_form/login');
	}
	
	function login()
	{
		$this->load->view('test_form/login');
	}

	function login_mobile()
	{
		$this->load->view('test_form/login_mobile');
	}
	
	function login_process()
	{
		$nip = $this->input->post('nip');
		$password = $this->input->post('password');
		
		$check_login = $this->access->login_process($nip,$password);
		
		if($check_login == TRUE)
		{
			echo "Yes , I'm Login <br/><a href='".base_url()."test_form/access_web/logout'>Logout</a>";
		}
		else
		{
			echo "Sorry, your can't access this <br/><a href='".base_url()."test_form/access_web'>Back to Login</a>";
		}	
	}
	
	function login_process_mobile()
	{
		$nip = $this->input->post('nip');
		$password = $this->input->post('password');
		
		$check_login = $this->access->login_process($nip,$password);
		
		if($check_login == TRUE)
		{
			$status= array('login' => 1);
		}
		else
		{
			$status= array('login' => 0);
		}	
		
		echo json_encode($status);
	}
	
	function logout()
	{
		$this->access->logout();
		$this->load->view('test_form/login');
	}

}	