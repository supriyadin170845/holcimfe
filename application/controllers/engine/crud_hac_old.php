<?php

class Crud_hac extends CI_controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->library('grocery_crud');	
	}

	function index()
	{
		$crud = new grocery_CRUD();
		
        $crud->set_table('hac');
        $crud->set_subject('HAC');

		$crud->set_field_upload('image','media/images');
		$crud->change_field_type('hac_id','hidden');
		
		$crud->fields('hac_id','hac_code','equipment','description','func_loc','description_loc','indicator','object_type','maker_type','planning_plant','image','assembly');
		
		$crud->callback_add_field('assembly',array($this,'assembly_form'));
		
		$crud->set_field_upload('image','media/images');
		
		$crud->unset_texteditor('description_loc','description','maker_type');
		
		$crud->columns('hac_id','hac_code');
	   
        $output = $crud->render();
 
        $this->output($output);
	}

	
	function assembly_form()
	{
		$assembly_form = '	<p><button class="btn btn-success add_field_assembly" type="button">Add Assembly <i class="icon-plus icon-white"></i></button><p/>
							<div class="row-fluid">
							<div class="span12">
							<table class="table table-bordered">
								<tbody class="form_field_assembly">
								
								</tbody>
							</table>
							</div>
							</div>
							';
		
		return $assembly_form ;
	}	
	
	function add_one_blank_record_assembly()
	{
		$this->db->insert('hac_assembly',array('assembly_id' => ''));
		
		$data['assembly_id'] = mysql_insert_id();
		
		$this->load->view('mod/get_blank_record_assembly',$data);
	}
	
	function output($output = null)
    {
        $this->load->view('page_crud_hac.php',$output);    
    }
}	