<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['shop_product_atribute'] = array('weight'); // You can set variable EX 'product_color','product_brands' //
$config['shop_theme'] = 'babythemes';

$config['shop_weight_format'] = 'kgs'; // Set value weight format EX: ons, kgs // 

// msessage 
$config['shop_add_cart_success'] = 'Product has added to cart';
$config['shop_update_cart_success'] = 'Your cart has updated';
$config['shop_delete_cart_success'] = 'Delete product from cart success';
$config['shop_login_user_not_found'] = 'Login failed, please fill your email and password correctly';
$config['shop_login_not_filled'] = 'Please fill some required fields';