$(function(){ 
    $('ul#slider li').hide(); // hide all list
    $('ul#slider li:first').fadeIn("slow"); // get first list  and show it
    rotate(); // do ratation with funtion rotation
});

function rotate(){
    var current = $('ul#slider li:visible'); // get the current list
    var next = current.next().length ? current.next() : current.parent().children(':first'); // do conditional
    current.hide(); // hide the current list
    next.fadeIn("slow"); // show next
    setTimeout(rotate, 10000); // Executes a code snippet or a function after specified delay (3000).
}