function load_pagging(page,div){
    $.ajax({
        type:"post",
        url: page,         
        success: function(response){ 
            $(div).html(response); 
        },
        error: function(x,h,r){
            alert(r)
        }
    });
    return false;
}
function font_plus(div){
    var cfs = $(div).css('font-size');
    var cfzn = parseFloat(cfs, 10);
    var newFontSize = cfzn*1.2;
    $(div).css('font-size', newFontSize);
    return false
}
function font_min(div){
    var cfs = $(div).css('font-size');
    var cfzn = parseFloat(cfs, 10);
    var newFontSize = cfzn*0.8;
    $(div).css('font-size', newFontSize);
    return false
}
function initMoving(target, position, topLimit) {
  if (!target)
  return false;

  var obj = target;
  obj.initTop = position;
  obj.topLimit = topLimit;

  obj.style.position = "absolute";
  obj.top = obj.initTop;
  obj.left = obj.initLeft;

  if (typeof(window.pageYOffset) == "number") {	//WebKit
    obj.getTop = function() {
      return window.pageYOffset;
    }
  } else if (typeof(document.documentElement.scrollTop) == "number") {
    obj.getTop = function() {
      return Math.max(document.documentElement.scrollTop, document.body.scrollTop);
    }
  } else {
    obj.getTop = function() {
      return 0;
    }
  }

  if (self.innerHeight) {	//WebKit
    obj.getHeight = function() {
      return self.innerHeight;
    }
  } else if(document.documentElement.clientHeight) {
    obj.getHeight = function() {
      return document.documentElement.clientHeight;
    }
  } else {
    obj.getHeight = function() {
      return 500;
    }
  }

  obj.move = setInterval(function() {
    if (obj.initTop > 0) {
      pos = obj.getTop() + obj.initTop;
    } else {
      pos = obj.getTop() + obj.getHeight() + obj.initTop;
      //pos = obj.getTop() + obj.getHeight() / 2 - 15;
    }

    if (pos < obj.topLimit)
    pos = obj.topLimit;
    if (pos > Math.max(document.documentElement.clientHeight, document.body.clientHeight))
    pos = Math.max(document.documentElement.clientHeight, document.body.clientHeight) - 1000;

    calc_pos = pos - topLimit;
    interval = obj.top - calc_pos ;
    if (calc_pos < 0) {
      obj.top = 0;
      obj.style.top = 0 + "px";
    } else {
      obj.top = obj.top - interval / 10;
      obj.style.top = obj.top + "px";
    }
  }, 30)
}
