
this.vshot = function(){
    xOffset = 30; // horizontal position
    yOffset = 10; // vertical position
    $("a.vshot").hover(function(e){ // do function when hover
        more=$(this).attr('more');
        this.t = this.title;
        url = this.id;// get title of attribute selected

        var d = this.name; // get name of attribute selected
        var c = (this.t != "") ? "<br/>" + this.t : "";
        var rel=this.rel;
        $("p#vshot").remove();
        $("body").append("<div id='vshot' style='max-width:660px'><span id='data'>\n\
        <img src='"+ rel +"' style='max-width:300px' align='left' alt='url preview' />\n\
        <div id='more_popup'>"+more+"</div>\n\
        "+d+"</span>\n\
        </div>");

        $("div#vshot").css({
            "top" : (e.pageY + xOffset) + "px",
            "left":(e.pageX + yOffset) + "px",
            "z-index":10,
            "position":"absolute",
            "border-radius": "5px",
            "-moz-border-radius": "5px",
            "-webkit-border-radius": "5px",
            "background": "#FFFFFF",
            "-webkit-box-shadow": "0 3px 16px rgba(0, 0, 0, 0.3)",
            "-moz-box-shadow":"1px 3px 16px rgba(0, 0, 0, 0.3)",
            "box-shadow": "0 3px 16px rgba(0, 0, 0, 0.3)",
            "padding":"10px",
            "display":"none",
            "color":"#666",
            "text-align":"left"
        }).fadeIn("fast"); 
    },function(){
        $("div#vshot").remove();
    });
};

$(document).ready(function(){
    vshot();
    $("div#vshot").hover(function(){},function(){
        $(this).remove();
    });
});