
function FileBrowser (field_name, url, type, win) {
    var cmsURL = site+'tinymce/additional/bmsupl.php';
    if (cmsURL.indexOf("?") < 0) {
        cmsURL = cmsURL + "?type=" + type;
    }else {
        cmsURL = cmsURL + "&type=" + type;
    }
    tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Image Browser',
        width : 420,  // Your dimensions may differ - toy around with them!
        height : 400,
        resizable : "yes",
        inline : "yes",  // This parameter only has an effect if you use the inlinepopups plugin!
        close_previous : "no"
    }, {
        window : win,
        input : field_name
    });
    return false;
}  
function slide_acount(i){
    if($(i).attr("class")=="show"){
        $("#det_acount").show("slow");
        $(i).removeClass("show").addClass("hide");
        $("span#ico-a").html("&Delta;");
    }else{
        $("#det_acount").hide("slow");
        $(i).removeClass("hide").addClass("show");
        $("span#ico-a").html("&nabla;");
    }
}
function cek_all(v){
    if($(v).attr("id")=='1'){
        $("input#cek[type='checkbox']").attr('checked',1);
        $(v).attr("id",'0')
    }else{
        $("input#cek[type='checkbox']").attr('checked',0).removeAttr('checked');
        $(v).attr("id",'1')
    }
    
}

function get_selected(i){
    $("ul#master_menu li a").removeClass("selected");
    $(i).addClass("selected");
}
function get_tipe(url,id,div){
    vid=$(id).val();
    small_load(url+vid,div);
}
function action_publish(page,i){
    $.ajax({
        type:"POST",
        url: site+page,
        success: function(response){ 
            $(i).replaceWith(response);
        },
        error:function(xhr){
            alert(xhr)
        }
    });
    return false;
}
function show_list(div){
    $("#show_max option").click(function(){
        val=$(this).html();
        page=site+'setting/page_view/'+val
        $.ajax({
            url: page,
            success: function(response){
                small_load(div,'div#cen_right') 
            },
            dataType:"html"
        });
    })

}
function show_list_(div){
    $("#show_max option").click(function(){
        val=$(this).html();
        page=site+'setting/page_view/'+val
        $.ajax({
            url: page,
            success: function(response){
                small_load(div,'div#center')
            },
            dataType:"html"
        });
    })

}
function load(page,div){
    $.ajax({
        url: site+page,
        beforeSend: function(){
            loading();
        },
        success: function(response){
            close_box();
            $(div).html(response);
        }
    });
    return false;
}
function backup(i,page){
    table=$(i).val();
    $.ajax({
        url: site+page+"/"+table,
        beforeSend: function(){
            loading();
        },
        success: function(response){
            alert("export table  "+table+" berhasil")
            load('backup/index/', '#cen_right');
        }
    });
    return false;
}

function small_load(page,div){
    $.ajax({
        url: site+page,
        beforeSend: function(){
            loading();
        },
        success: function(response){
            $(div).html(response);
            close_box();
        }
    });
    return false;
}
function load_pagging(page,div){
    $.ajax({
        type:"post",
        url: page,         
        success: function(response){
            close_box();
            $(div).html(response); 
        },
        error: function(xhr){
            alert(xhr)
        }
    });
    return false;
}
function notive(txt){
    $("div#notive").html(txt).css({
        "background": "#F9EDBE",
        "border": "1px solid #F0C36D"
    }).click(function(){
        $(this).html("").css({
            "background": "transparent",
            "border": "none"
        })
    }).fadeIn("slow");
}
function small_update_action(page){
    var myForm = $("#cen_right form");
    myForm.validation();
    if(!myForm.validate()) {

    }else{
        data = $("#cen_right form").serialize()
        $.ajax({
            type:"POST",
            beforeSend: function(){
                loading();
            },
            url: site+page,
            data : data,
            success: function(response){
                close_box();
                notive("thanks to update data...")
                $("#cen_right form input, #cen_right form select, #cen_right form textarea").val('');
                var ui=page.split('/');
                load(ui['0'],"#cen_right")
            }
        });
    }
    return false;
}

function small_post_action(page){
    var myForm = $("#cen_right form");
    myForm.validation();
    if(!myForm.validate()) {
    }else{
        data = $("#cen_right form").serialize()
        $.ajax({
            type:"POST",
            beforeSend: function(){
            //loading();
            },
            url : site+page,
            data : data,
            success : function(response){ 
                close_box();
                notive("thanks to submit data...")
                $("#cen_right form input, #cen_right form select, #cen_right form textarea").val('');
                var ui=page.split('/');
                load(ui['0'],"#cen_right")
            },
            error: function(xhr,x,c){
                alert(c)
            }
        })
    }
    return false;
}

function update_view(page,div){ 
    field = document.getElementsByTagName("input");
    for (i = 0; i < field.length; i++){
        if(field[i].checked == true){ 
            $.ajax({
                type:"POST",
                beforeSend: function(){
                    loading();
                },
                url: site+page+"/",
                data : $("#cen_right form").serialize(),
                success: function(response){
                    close_box();
                    $(div).html(response)
                    
                }
            });
        }else{           
            
        }
    }
    return false;
}

function loading(){
    var t=$(window).height();
    var tt= $(".light_box").height();
    var top=((t-tt)/3);
    var l=$("body").width();
    var left=((l-450)/2);
    cmd= "<div class='light_box' style='padding:0px; background: transparent;  z-index:1000; margin-left:150px;' id='page_loading'>"+
    "<div class='light_content' style='background: transparent;' align='center'>"+loadImg+"<br/> &nbsp;LOADING.........</div>"+
    "</div>";
    $("#wrapper").css({
        opacity:0.1
    })
    $("body").append(cmd)
    $(".light_box").css({
        left: left,
        top: top
    })
}

function close_box(){
    $("div.light_box").remove();
    $("#wrapper").css({
        opacity:1
    })
}
function lightbox(page){
    $.ajax({
        beforeSend: function(){
            loading();
        },
        url: site+page,
        success: function(response){
            $(".light_box").remove();
            $("body").append(
                "<div class=\"light_box\">"+
                "<div class='light_content'></div>"+
                "</div>"
                )
            $(".light_content").append(response)
            var l=$("body").width();
            var left=((l-$(".light_box").width())/2);
            var h=$(window).height();
            var height=((h-$(".light_box").height())/2);
            $(".light_box").css({
                left: left,
                top: height,
                "z-index":10
            })
            $('.loading').html('');
            $("#wrapper").css({
                opacity:0.1
            })
        },
        error: function(xhr){
            alert(xhr)
        },
        dataType:"html"
    });

    return false;
}


function show_child(cls){
    stat=$(cls).css("display");
    if(stat=='none'){
        $(cls).show()
    }else{
        $(cls).hide();
    }
}

function action_remove(page){
    $.ajax({
        type:"POST", 
        url: site+page,
        data : $("#cen_right form").serialize(),
        success: function(response){
            $(response).replaceWith('');
            close_box();
            notive("data has removed...")
        },
        dataType:"html"
    });
    return false;
}

function table_sort(page){
    $("table#table_list tbody").sortable({
        opacity: 0.6,
        cursor: 'move',
        update: function() {
            var order = $(this).sortable("serialize");
            $.ajax({
                type:"POST",
                url: site+page,
                data: order,
                success: function(response){ 
                },
                dataType:"html"
            });
        }
    });
}
function search_field(i,page){
    value=$(i).prev().val();
    ch=$("select#id_search option:selected").val(); 
    small_load(page+'/'+ch+'/'+value,'div#cen_right');
}

function search_field_view(i,page){ 
    cat=$("select#id_cat option:selected").val();
    kind=$("select#id_kind option:selected").val();    
    small_load(page+'/'+cat+'/'+kind,'div#left');
}
function order_field(page){
    small_load(page,'div#cen_right');
}
function show_login(i){
    $("div#login_slide").show();
    if($("div#login_slide").css("display")=="none"){
        $(i).css({
            background:"transparent",
            "color":"#666"
        })
    }else{
        $(i).css({
            background:"#FFF",
            "color":"#666"
        })
    }
    $("div#login_slide").hover(function(){},function(){
        $(i).css({
            background:"transparent",
            "color":"#FFF"
        })
        $(this).hide();
    })
}
function f_tab(i,s,h){
    $("div#f_tab a").removeClass("selected");
    $(i).addClass("selected");
    $(s).show()
    $(h).hide()
}